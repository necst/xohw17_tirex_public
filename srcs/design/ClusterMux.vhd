----------------------------------------------------------------------------------
-- Company: Politecnico di Milano
-- Student: Alessandro Comodi
-- Student: Davide Conficconi
-- 
-- Create Date: 03/21/2017 03:56:02 PM
-- Design Name: TiReX - single core
-- Module Name: ClusterMux - struct
-- Project Name: TiReX
-- Target Devices: Virtex 7 series
-- Tool Versions: Vivado 16.4
-- Description: This module comprehends the various clusters and a multiplexer.
--				The multiplexer selects the right input for the clusters:
--					- 	if the opcode is an OR then each cluster will be provided
--						with only one character to be matched simultaneously by
--						all the comparators
--					-	if the opcode is an AND each comparator will have different
--						data and will work in parallel
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use work.typesPack.all;

entity ClusterMux is
	generic(
		ClusterWidth		: positive := 2;
		NCluster			: positive := 1;
		DataWidth			: positive := 8
		);
	port(
		-- signal to indicate if the operator is an or. HIGH it is an or, LOW it is an AND
		opcode_is_or		: in std_logic;
		-- signal to enable the comparators. HIGH comparators are disabled, LOW they are enabled
		en 					: in std_logic;
		-- data to be given to the comparators and to be checked
		data 				: in ClusterArray(0 to ClusterWidth - 1);
		-- reference to do the comparison with the data
		reference 			: in ClusterArray(0 to ClusterWidth - 1);
		-- results of the cluster
		result 				: out std_logic_vector(ClusterWidth - 1 downto 0)
		);
end ClusterMux;
	
architecture struct of ClusterMux is
	signal data_i : ClusterArray(0 to ClusterWidth - 1) := (others => (others => '0'));
begin
	
	generate_mux		: for i in 0 to ClusterWidth - 1 generate
		Mux 	: entity work.Mux
			generic map(
				DataWidth 		=> DataWidth
				)
			port map(
				data_in_1		=> data(i),
				data_in_2		=> data(0),
				sel 			=> opcode_is_or,
				data_out		=> data_i(i)
				);
	end generate generate_mux; 
	
	generate_clusters	: for i in 0 to NCluster - 1 generate
		Cluster : entity work.Cluster
			generic map(
				ClusterWidth	=> ClusterWidth,
				DataWidth 		=> DataWidth
				)
			port map(
				en 				=> en,
				data 			=> data_i,
				reference		=> reference,
				result 			=> result
				);
	end generate generate_clusters;
end struct;
