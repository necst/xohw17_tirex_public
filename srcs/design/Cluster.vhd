----------------------------------------------------------------------------------
-- Company: Politecnico di Milano
-- Student: Alessandro Comodi
-- Student: Davide Conficconi
-- 
-- Create Date: 03/21/2017 03:56:02 PM
-- Design Name: TiReX - single core
-- Module Name: Cluster - struct
-- Project Name: TiReX
-- Target Devices: Virtex 7 series
-- Tool Versions: Vivado 16.4
-- Description: This module generates all the comparators depending on the
--				ClusterWidth parameter.
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use work.typesPack.all;

entity Cluster is
	generic(
		ClusterWidth		: positive := 2;
		DataWidth			: positive := 8
		);
	port(
		-- Signal to enable or disable the comparators. If HIGH comparators
		-- are disabled, if LOW comparators are enabled
		en 					: in std_logic;
		-- data to check
		data 				: in ClusterArray(0 to ClusterWidth - 1);
		-- reference data for the comparison
		reference 			: in ClusterArray(0 to ClusterWidth - 1);
		-- vector of results. Each bit correspond to a comparator
		result				: out std_logic_vector(0 to ClusterWidth - 1)
		);
end Cluster;

architecture struct of Cluster is
begin
	comp_gen : for i in 0 to ClusterWidth - 1 generate
		Comparator : entity work.CompareUnit
			generic map(
				DataWidth	=> DataWidth
				)
			port map(
				en 			=> en,
				data 		=> data(i),
				reference   => reference(i),
				result 		=> result(i)	
				);
	end generate comp_gen;
end struct;
