----------------------------------------------------------------------------------
-- Company: Politecnico di Milano
-- Student: Alessandro Comodi
-- Student: Davide Conficconi
-- 
-- Create Date: 03/21/2017 03:56:02 PM
-- Design Name: TiReX - single core
-- Module Name: TirexCore - behav
-- Project Name: TiReX
-- Target Devices: Virtex 7 series
-- Tool Versions: Vivado 16.4
-- Description: This single Core is a customized processor(with a custom ISA)
--				that taking a stream of data and a sequence of instruction,
--				scan the stream for a match of a regular expression inside it.
--				It needs the start and end address of data, an enable of research,
--				data and instruction memory. How the instruction are encoded is
--				taken into account by the compiler provided with the core.
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use work.OpcodePack.all;

entity TirexCore is
	generic(
		AddressWidthInstr	: positive := 4;
		AddressWidthData	: positive := 4;
		OpCodeBus			: positive := 10;
		OpCodeWidth			: positive := 6;
		CharacterNumber		: positive := 14;
		InternalOpBus		: positive := 2;
		DataWidth			: positive := 8;
		ClusterWidth		: positive := 2;
		BusWidthData		: positive := 32;
		RamWidthInstr		: positive := 18;

		CounterWidth		: positive := 3;
		StackDataWidth		: positive := 17;
		BufferAddressWidth : positive := 3;

		NCluster			: positive := 1
		);
	port(
		clk					: in std_logic;
		rst					: in std_logic;

		addr_start_instr	: in std_logic_vector(AddressWidthInstr - 1 downto 0);
		first_character		: in std_logic_vector(CharacterNumber - 1 downto 0);
		ending_character	: in std_logic_vector(CharacterNumber - 1 downto 0);

		curr_character	 	: out std_logic_vector(CharacterNumber - 1 downto 0);
		curr_last_match_char: out std_logic_vector(CharacterNumber - 1 downto 0);
		reload_bram			: out std_logic_vector(1 downto 0);
		reload_complete		: in std_logic;

		src_en 				: in std_logic;
		complete			: out std_logic;
		found				: out std_logic;
		where_complete		: out std_logic_vector(2 downto 0);

		addr_instruction_1	: out std_logic_vector(AddressWidthInstr - 1 downto 0);
		addr_instruction_2	: out std_logic_vector(AddressWidthInstr - 1 downto 0);
		instruction_1 		: in std_logic_vector(RamWidthInstr - 1 downto 0);
		instruction_2 		: in std_logic_vector(RamWidthInstr - 1 downto 0);

		addr_instruction_3 	: out std_logic_vector(AddressWidthInstr - 1 downto 0);
		instruction_3 		: in std_logic_vector(RamWidthInstr - 1 downto 0);

		addr_data 	 		: out std_logic_vector(AddressWidthData - 1 downto 0);
		data 				: in std_logic_vector(BusWidthData - 1 downto 0)
		);  
end TirexCore;

architecture behav of TirexCore is

	constant FDSel				: positive := 2;


	------Internal Signals needed to link the different components ----------------
	signal fd_op_code_a			: std_logic_vector(OpCodeBus - 1 downto 0);
	signal fd_instr_data_a		: std_logic_vector(ClusterWidth * DataWidth - 1 downto 0);
	signal fd_valid_ref_a		: std_logic_vector(ClusterWidth - 1 downto 0);

	signal fd_op_code_b			: std_logic_vector(OpCodeBus - 1 downto 0);
	signal fd_instr_data_b		: std_logic_vector(ClusterWidth * DataWidth - 1 downto 0);
	signal fd_valid_ref_b		: std_logic_vector(ClusterWidth - 1 downto 0);

	signal fd_op_code_c			: std_logic_vector(OpCodeBus - 1 downto 0);
	signal fd_instr_data_c		: std_logic_vector(ClusterWidth * DataWidth - 1 downto 0);
	signal fd_valid_ref_c		: std_logic_vector(ClusterWidth - 1 downto 0);

	signal reg_op_code_a		: std_logic_vector(OpCodeBus - 1 downto 0);
	signal reg_instr_data_a		: std_logic_vector(ClusterWidth * DataWidth - 1 downto 0);
	signal reg_valid_ref_a		: std_logic_vector(ClusterWidth - 1 downto 0);

	signal reg_op_code_b		: std_logic_vector(OpCodeBus - 1 downto 0);
	signal reg_instr_data_b		: std_logic_vector(ClusterWidth * DataWidth - 1 downto 0);
	signal reg_valid_ref_b		: std_logic_vector(ClusterWidth - 1 downto 0);

	signal reg_op_code_c		: std_logic_vector(OpCodeBus - 1 downto 0);
	signal reg_instr_data_c		: std_logic_vector(ClusterWidth * DataWidth - 1 downto 0);
	signal reg_valid_ref_c		: std_logic_vector(ClusterWidth - 1 downto 0);

	signal mux_opcode			: std_logic_vector(OpCodeBus - 1 downto 0);
	signal mux_instr_data		: std_logic_vector(ClusterWidth * DataWidth - 1 downto 0);
	signal mux_valid_ref		: std_logic_vector(ClusterWidth - 1 downto 0);

	signal ex_data_offset		: std_logic_vector(AddressWidthData - 1 downto 0);
	signal ex_match				: std_logic;
	signal ex_vr_bin			: std_logic_vector(ClusterWidth - 1 downto 0);
	signal ex_cluster_match		: std_logic_vector(NCluster - 1 downto 0);

	signal jump_offset_i 		: std_logic_vector(ClusterWidth * DataWidth - 1 downto 0);
	signal jump_flag_i			: std_logic;

	signal cp_sel_fd			: std_logic_vector(FDSel - 1 downto 0);
	signal cp_en_comparator		: std_logic_vector(NCluster - 1 downto 0);
	signal cp_stall				: std_logic;
	
begin


	FETCH_DECODE_A	: entity work.FetchDecode
		generic map(
			ClusterWidth 		=> ClusterWidth,
			OpCodeBus			=> OpCodeBus,
			OpCodeWidth 		=> OpCodeWidth,
			DataWidth			=> DataWidth
			)
		port map(
			instruction 		=> instruction_1,
			op_code_out			=> fd_op_code_a,
			valid_ref 			=> fd_valid_ref_a,
			instr_data 			=> fd_instr_data_a
			);

	FETCH_DECODE_B	: entity work.FetchDecode
		generic map(
			ClusterWidth 		=> ClusterWidth,
			OpCodeBus			=> OpCodeBus,
			OpCodeWidth 		=> OpCodeWidth,
			DataWidth			=> DataWidth
			)
		port map(
			instruction 		=> instruction_2,
			op_code_out			=> fd_op_code_b,
			valid_ref 			=> fd_valid_ref_b,
			instr_data 			=> fd_instr_data_b
			);

	FETCH_DECODE_C	: entity work.FetchDecode
		generic map(
			ClusterWidth 		=> ClusterWidth,
			OpCodeBus			=> OpCodeBus,
			OpCodeWidth 		=> OpCodeWidth,
			DataWidth			=> DataWidth
			)
		port map(
			instruction 		=> instruction_3,
			op_code_out			=> fd_op_code_c,
			valid_ref 			=> fd_valid_ref_c,
			instr_data 			=> fd_instr_data_c
			);

	-------------------
	OPCODE_REG_A		: entity work.Reg
		generic map(
			DataWidth			=> OpCodeBus
			)
		port map(
			clk 				=> clk,
			rst 				=> rst,
			data_in 			=> fd_op_code_a,
			data_out			=> reg_op_code_a
			);

	VALID_REF_REG_A	: entity work.Reg
		generic map(
			DataWidth			=> ClusterWidth
			)
		port map(
			clk 				=> clk,
			rst 				=> rst,
			data_in 			=> fd_valid_ref_a,
			data_out			=> reg_valid_ref_a
			);

	INSTR_DATA_REG_A	: entity work.Reg
		generic map(
			DataWidth			=> ClusterWidth * DataWidth
			)
		port map(
			clk 				=> clk,
			rst 				=> rst,
			data_in 			=> fd_instr_data_a,
			data_out			=> reg_instr_data_a
			);


---------------------------------------------------------	

	
	OPCODE_REG_B		: entity work.Reg
		generic map(
			DataWidth			=> OpCodeBus
			)
		port map(
			clk 				=> clk,
			rst 				=> rst,
			data_in 			=> fd_op_code_b,
			data_out			=> reg_op_code_b
			);

	VALID_REF_REG_B	: entity work.Reg
		generic map(
			DataWidth			=> ClusterWidth
			)
		port map(
			clk 				=> clk,
			rst 				=> rst,
			data_in 			=> fd_valid_ref_b,
			data_out			=> reg_valid_ref_b
			);

	INSTR_DATA_REG_B	: entity work.Reg
		generic map(
			DataWidth			=> ClusterWidth * DataWidth
			)
		port map(
			clk 				=> clk,
			rst 				=> rst,
			data_in 			=> fd_instr_data_b,
			data_out			=> reg_instr_data_b
			);

-----------------------------------------------------------
	
	OPCODE_REG_C		: entity work.Reg
		generic map(
			DataWidth			=> OpCodeBus
			)
		port map(
			clk 				=> clk,
			rst 				=> rst,
			data_in 			=> fd_op_code_c,
			data_out			=> reg_op_code_c
			);

	VALID_REF_REG_C	: entity work.Reg
		generic map(
			DataWidth			=> ClusterWidth
			)
		port map(
			clk 				=> clk,
			rst 				=> rst,
			data_in 			=> fd_valid_ref_c,
			data_out			=> reg_valid_ref_c
			);

	INSTR_DATA_REG_C	: entity work.Reg
		generic map(
			DataWidth			=> ClusterWidth * DataWidth
			)
		port map(
			clk 				=> clk,
			rst 				=> rst,
			data_in 			=> fd_instr_data_c,
			data_out			=> reg_instr_data_c
			);

-----------------------------------------------------------


	FD_MUX_OPCODE 			: entity work.Mux3In
		generic map(
			DataWidth 			=> OpCodeBus,
			SelWidth			=> FDSel
			)
		port map(
			data_in_1 			=> reg_op_code_a,
			data_in_2			=> reg_op_code_b,
			data_in_3			=> reg_op_code_c,
			sel 				=> cp_sel_fd,
			data_out			=> mux_opcode

			);

	FD_MUX_VAL_REF 			: entity work.Mux3In
		generic map(
			DataWidth 			=> ClusterWidth,
			SelWidth			=> FDSel
			)
		port map(
			data_in_1 			=>reg_valid_ref_a,
			data_in_2			=>reg_valid_ref_b,
			data_in_3			=>reg_valid_ref_c,
			sel 				=>cp_sel_fd,
			data_out			=>mux_valid_ref

			);

	FD_MUX_INS_DATA 			: entity work.Mux3In
		generic map(
			DataWidth 			=> ClusterWidth * DataWidth,
			SelWidth			=> FDSel
			)
		port map(
			data_in_1 			=>reg_instr_data_a,
			data_in_2			=>reg_instr_data_b,
			data_in_3			=>reg_instr_data_c,
			sel 				=>cp_sel_fd,
			data_out			=>mux_instr_data

			);
--------------------------------------------------------


	EXECUTE			: entity work.Execute
		generic map(
			InternalOpBus		=> InternalOpBus,
			ClusterWidth  		=> ClusterWidth,
			NCluster			=> NCluster,
			DataWidth			=> DataWidth,
			AddressWidthData 	=> AddressWidthData,
			BusWidthData		=> BusWidthData
			)
		port map(
			clk					=> clk,
			rst 				=> rst,
			en_comparators		=> cp_en_comparator,
			data				=> data,
			data_offset 		=> ex_data_offset,
			reference			=> mux_instr_data,
			match 				=> ex_match,
			valid_ref			=> mux_valid_ref,
			vr_bin 				=> ex_vr_bin,
			operator			=> mux_opcode(InternalOpBus - 1 downto 0),
			cluster_match 		=> ex_cluster_match,
			stall 				=> cp_stall
			);

	CONTROL_PATH	: entity work.ControlPath
		generic map(
			AddrWidthInstr 		=> AddressWidthInstr,
			AddrWidthData 		=> AddressWidthData,
			CharacterNumber		=> CharacterNumber,
			OpBusWidth 			=> OpCodeBus,
			InternalOpBus 		=> InternalOpBus,

			CounterWidth		=> CounterWidth,
			BufferAddressWidth 	=> BufferAddressWidth,
			StackDataWidth 		=> StackDataWidth,

			DataWidth			=> DataWidth,

			ClusterWidth		=> ClusterWidth,
			NCluster			=> NCluster
			)
		port map(
			clk 				=> clk,
			rst 				=> rst,
			src_en 				=> src_en,
			addr_start_instr	=> addr_start_instr,
			
			first_character		=> first_character,
			ending_character	=> ending_character,

			curr_character		=> curr_character,
			curr_last_match_char=> curr_last_match_char,
			reload_bram			=> reload_bram,
			reload_complete		=> reload_complete,

			cluster_match 		=> ex_cluster_match,

			found 				=> found,
			complete 			=> complete,
			where_complete		=> where_complete,

			jump_offset			=> jump_offset_i,
			jump_flag 			=> jump_flag_i,

			addr_instruction_1	=> addr_instruction_1,
			addr_instruction_2	=> addr_instruction_2,

			addr_instruction_3	=> addr_instruction_3,
			
			addr_data 	 		=> addr_data,
			sel_fd 				=> cp_sel_fd,
			op_code 			=> mux_opcode,
			match 				=> ex_match,
			data_offset 		=> ex_data_offset,
			enable_comparators	=> cp_en_comparator,
			stall 				=> cp_stall
			);

		Offset : process(mux_opcode(op_jmp))
		begin
			if mux_opcode(op_jmp) = '1' then

				jump_offset_i <= mux_instr_data;
			else
				jump_offset_i <= (others => '0');
			end if ;
			
		end process ; -- Offset

		jmp_flag : process( mux_opcode(op_opar) )
		begin
			if mux_opcode(op_opar) = '1' then
				jump_flag_i <= mux_instr_data(0);
			else
				jump_flag_i <= '0';
			end if ;
		end process ; -- jmp_flag
end behav;
