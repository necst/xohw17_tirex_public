----------------------------------------------------------------------------------
-- Company: Politecnico di Milano
-- Engineer: Alessandro Comodi
-- Engineer: Davide Conficconi
-- 
-- Create Date: 03/21/2017 03:56:02 PM
-- Design Name: TiReX - single core
-- Module Name: TypesPack - package
-- Project Name: TiReX
-- Target Devices: Virtex 7 series
-- Tool Versions: Vivado 16.4
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use work.GenericsPack.all;

package TypesPack is

    -- array of std_logic_vector (width = DataWidth) to map a wider std_logic_vector into
    -- the registers of the comparators
    type RegisterArray is array (natural range <>) of std_logic_vector (DataWidth - 1 downto 0);

    subtype ClusterArray is RegisterArray;

end TypesPack;

package body TypesPack is
end TypesPack;

