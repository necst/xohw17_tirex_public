----------------------------------------------------------------------------------
-- Company: Politecnico di Milano
-- Student: Alessandro Comodi
-- Student: Davide Conficconi 
-- Create Date: 03/31/2017 07:34:14 PM
-- Design Name: TiReX - single core
-- Module Name: RamInstr - behav
-- Project Name: TiReX
-- Target Devices: Virtex 7 series 
-- Tool Versions: Vivado 16.4
-- Description: This component is the Ram dedicated to the storage of the instructions.
--              The instructions will be passed to the Fetch and Decode unit which will
--              extract the significant data for the computation.
--              This Ram is not included in the TiReX core, but it is external 
--              to it.
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity RamInstr is
    generic
      (
      -- Number of bits that defines the length of the address. The DataRam
       -- will have 2^AddressWidth cells
      BusWidth      : positive := 32;
      DataWidth     : positive := 8;
      OpCodeWidth   : positive := 6;
      AddressWidth  : positive := 4;
      RamWidth      : positive := 38;
      ClusterWidth  : positive := 4
      );
  port
    (
      rst 		      : in std_logic;
      clk           : in std_logic;
      data_in       : in std_logic_vector(BusWidth - 1 downto 0);
      address_rd_1  : in std_logic_vector(AddressWidth - 1 downto 0);
      address_rd_2  : in std_logic_vector(AddressWidth - 1 downto 0);
      address_rd_3  : in std_logic_vector(AddressWidth - 1 downto 0);
      address_wr    : in std_logic_vector(AddressWidth - 1 downto 0);

      we_data       : in std_logic;
      we_opcode     : in std_logic;
      data_out_1 	  : out std_logic_vector(RamWidth - 1 downto 0);
      data_out_2    : out std_logic_vector(RamWidth - 1 downto 0);
      data_out_3    : out std_logic_vector(RamWidth - 1 downto 0)

      );
end RamInstr;

architecture beh of RamInstr is
  type RAM is array(0 to 2 ** AddressWidth - 1) of std_logic_vector(RamWidth - 1 downto 0);

  signal b_ram : RAM;
begin
  data_out_1 <= b_ram(to_integer(unsigned(address_rd_1)));
  data_out_2 <= b_ram(to_integer(unsigned(address_rd_2)));
  data_out_3 <= b_ram(to_integer(unsigned(address_rd_3)));

  process (clk, rst)
  begin
    if rst = '0' then
			b_ram <= (others => (others => '0'));
    elsif rising_edge(clk) then
      if we_data = '1' then
        b_ram(to_integer(unsigned(address_wr)))(DataWidth * ClusterWidth - 1 downto 0) <= data_in(DataWidth * ClusterWidth - 1 downto 0);
      elsif we_opcode = '1' then
        b_ram(to_integer(unsigned(address_wr)))(RamWidth - 1 downto DataWidth * ClusterWidth) <= data_in(OpCodeWidth - 1 downto 0);
      end if;
    end if;
  end process;
end beh;