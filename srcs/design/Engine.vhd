----------------------------------------------------------------------------------
-- Company: Politecnico di Milano
-- Student: Alessandro Comodi
-- Student: Davide Conficconi
-- 
-- Create Date: 03/21/2017 03:56:02 PM
-- Design Name: TiReX - single core
-- Module Name: Engine - behav
-- Project Name: TiReX
-- Target Devices: Virtex 7 series
-- Tool Versions: Vivado 16.4
-- Description: This component collect all the result of the clusters and the
--				comparators, produce an intermediate result according to the
--				opcode, produce a valid offset from which the data has to be
--				fetched and a binary representation of the valid characters
--				in the instruction
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use ieee.numeric_std.all;
use work.opcodePack.all;

entity Engine is
	generic(
		InternalOpBus		: positive := 2;
		NCluster			: positive := 1;
		ClusterWidth		: positive := 2;
		AddressWidthData	: positive := 4
		);
	port(
		-- A vector that contain the single cluster result
		compare_results 	: in std_logic_vector(ClusterWidth * NCluster - 1 downto 0); 
		-- Valid characters in the instruction
		valid_ref			: in std_logic_vector(ClusterWidth - 1 downto 0);
		-- Opcode of the instruction 
		operator			: in std_logic_vector(InternalOpBus - 1 downto 0);
		-- Data offset produced by the Address generator according to the operator and the valide ref 			 
		address_offset		: out std_logic_vector(AddressWidthData - 1 downto 0);
		-- Cycle result 		 
		match				: out std_logic;
		-- Binary representation of the valid ref							 
		vr_bin 				: out std_logic_vector(ClusterWidth - 1 downto 0);
		-- Vector of bits representing which cluster has done the match
		-- The cluster that matched correspond to the '0' in the vector.
		-- All the other values are set to '1'
		cluster_match		: out std_logic_vector(NCluster - 1 downto 0);
		-- Signal coming from the Control Path to stall or not the execution, active high		 
		stall				: in std_logic 												 
		);
end Engine;

architecture behav of Engine is
	signal valid_ref_bin_i 		: std_logic_vector(ClusterWidth - 1 downto 0);
	signal cluster_result  		: std_logic_vector(NCluster - 1 downto 0);

begin
	
	--------------------------------------------------- 
	-- Computation of the binary value of valide_ref --
	---------------------------------------------------
	valid_ref_bin_conv : process (valid_ref, operator)
    	variable valid_ref_bin : std_logic_vector(ClusterWidth - 1 downto 0) := (others => '0');
	begin
	    valid_ref_bin     := (others => '0');
	    for i in ClusterWidth - 1 downto 0 loop
		    if valid_ref(i) = '1' then
		   		valid_ref_bin := valid_ref_bin + 1;
		    end if;
	    end loop;
	    if operator(op_and) = '1' then
	    	valid_ref_bin_i <= valid_ref_bin;
	    elsif operator(op_or) = '1' then
	    	valid_ref_bin_i <= std_logic_vector(to_unsigned(1, ClusterWidth));
	    else 
	    	valid_ref_bin_i <= (others => '0');
	    end if;
	end process valid_ref_bin_conv;

	vr_bin <= valid_ref_bin_i;

	----------------------------------------------------
	-- Processing of the results by the comparators if the exectuion not stalled
	---------------------------------------------------
	process (compare_results, valid_ref, operator, stall)
		variable match_i : std_logic 								:= '0';
		variable match_a : std_logic_vector(NCluster - 1 downto 0) 	:= (others => '0');
	begin
		match <= '0';
		cluster_result <= (others => '0');
		match_i := '0';
		match_a := (others => '0');
		if stall = '0' then
			for j in 1 to NCluster loop
				match_a(NCluster - j) := compare_results(ClusterWidth * (NCluster - j + 1) - 1);
				for i in 2 to ClusterWidth loop
					if valid_ref(ClusterWidth - i) = '1' then
						if operator(op_or) = '1' then
							match_a(NCluster - j) := match_a(NCluster - j) or compare_results(ClusterWidth * (NCluster - j + 1) - i);
						elsif operator(op_and) = '1' then
							match_a(NCluster - j) := match_a(NCluster - j) and compare_results(ClusterWidth * (NCluster - j + 1) - i);
						end if;
					end if;
				end loop;
				match_i := match_i or match_a(NCluster - j);
			end loop;
		else
				match_i := '0';
		end if;

		cluster_result <= match_a;
		match 		   <= match_i;
	end process;

	---Producer of the right offest

	addr_generator : entity work.AddrGenerator
		generic map(
			AddressWidthData	=> AddressWidthData,
			NCluster			=> NCluster,
			ClusterWidth 		=> ClusterWidth
			)
		port map(
			cluster_result 		=> cluster_result,
			valid_ref_bin 		=> valid_ref_bin_i,
			data_offset			=> address_offset,
			cluster_match		=> cluster_match
			);
end behav;
