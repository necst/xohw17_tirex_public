----------------------------------------------------------------------------------
-- Company: Politecnico di Milano
-- Student: Alessandro Comodi
-- Student: Davide Conficconi
-- 
-- Create Date: 03/21/2017 03:56:02 PM
-- Design Name: TiReX - single core
-- Module Name: FetchDecode - behav
-- Project Name: TiReX
-- Target Devices: Virtex 7 series
-- Tool Versions: Vivado 16.4
-- Description: THis component has to process the instruction that arrives from
--				the instruction Ram. The F/D phase splits the instruction in: 
--					- opcode : 	the produced result is a one-hot vector with only
--								one bit at "1" corresponding to the decoded
--								operator
--					- instruction data : 	Referring to the reference characters
--											to be matched by the comparators	
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.OpcodePack.all;

entity FetchDecode is
	generic(
		-- Number of comparators in each cluster
		ClusterWidth	: positive := 2;
		-- Number of operators that are present in the one-hot vector
		OpCodeBus		: positive := 10;
		-- Number of bits of the instruction which represent the opcode
		OpCodeWidth		: positive := 6;
		-- Length of the data that has to be checked (Usually is 8 corresponding to the ASCII encoding)
		DataWidth		: positive := 8
		);
	port(
		-- Instruction semt by the Instruction Ram
		instruction		: in std_logic_vector((DataWidth * ClusterWidth + OpCodeWidth) - 1 downto 0);
		-- One-hot vector representing the decoded opcode
		op_code_out		: out std_logic_vector(OpCodeBus - 1 downto 0);
		-- Vector of bits: if a bit is "1" it means that the corresponding reference character is valid
		-- and has to be counted in the comparison
		valid_ref		: out std_logic_vector(ClusterWidth - 1 downto 0);
		-- Reference data decoded from the instruction
		instr_data		: out std_logic_vector((DataWidth * ClusterWidth) - 1 downto 0)
		);
end FetchDecode;

architecture behav of FetchDecode is
	constant zeros 		: std_logic_vector(DataWidth - 1 downto 0) := (others => '0');
	
	-- Internal Signals
	signal instr_data_i	: std_logic_vector((DataWidth * ClusterWidth) - 1 downto 0);
	signal op_code_i 	: std_logic_vector(OpCodeWidth - 1 downto 0);
begin
	-- Extraction of the instruction data to pass as reference to the Execute
	instr_data_i <= instruction((DataWidth * ClusterWidth) - 1 downto 0);
	instr_data 	<= instr_data_i;

	-- Extraction of the opcode and generation of the opcode one-hot vector
	op_code_i 	<= instruction((DataWidth * ClusterWidth + OpCodeWidth) - 1 downto (ClusterWidth * DataWidth));
	
	op_code_out(op_or)	 <= '1' when op_code_i(opIpos downto opIpos-opIWidth+1) = opc_or else '0';
	op_code_out(op_and)	 <= '1' when op_code_i(opIpos downto opIpos-opIWidth+1) = opc_and else '0';
	
	op_code_out(op_alws) <= '1' when op_code_i(opIpos downto opIpos-opIWidth+1) = opc_alws else '0';	


	op_code_out(op_cp_plus) <= '1' when op_code_i(opEpos downto opEpos-opEWidth+1) = opc_cp_plus else '0';
	op_code_out(op_cp_star) <= '1' when op_code_i(opEpos downto opEpos-opEWidth+1) = opc_cp_star else '0';
	op_code_out(op_cp_or) 	<= '1' when op_code_i(opEpos downto opEpos-opEWidth+1) = opc_cp_or else '0';
	op_code_out(op_cp) 		<= '1' when op_code_i(opEpos downto opEpos-opEWidth+1) = opc_cp      else '0';
	op_code_out(op_jmp)		<= '1' when op_code_i(opEpos downto opEpos-opEWidth+1) = opc_jmp else '0';
	op_code_out(op_opar) 	<= '1' when op_code_i(opParpos) = opc_opar else '0';
	op_code_out(op_nop)  	<= '1' when op_code_i = opc_nop else '0';

	-- This process calculates the valid_reference vector
	process (instruction)
	begin
		for i in 0 to ClusterWidth - 1 loop
			if instruction((DataWidth * (i + 1)) - 1 downto (DataWidth * i)) = zeros then
				valid_ref(i) <= '0';
			else
				valid_ref(i) <= '1';
			end if;
		end loop;
	end process;

end behav;