----------------------------------------------------------------------------------
-- Company: Politecnico di Milano
-- Student: Alessandro Comodi
-- Student: Davide Conficconi
-- 
-- Create Date: 03/21/2017 03:56:02 PM
-- Design Name: TiReX - single core
-- Module Name: AddrGenerator - behav
-- Project Name: TiReX
-- Target Devices: Virtex 7 series
-- Tool Versions: Vivado 16.4
-- Description: This module generates the offset to be added to the current
--              Data Buffer address in order to jump at the right batch of data.
--              It depends on the results of the clusters and on the valid
--              references.
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use ieee.numeric_std.all;

entity AddrGenerator is
  generic(
    AddressWidthData : positive := 4;
    NCluster         : positive := 1;
    ClusterWidth     : positive := 2
    );

  port(
    -- Vector of results of the clusters
    cluster_result : in  std_logic_vector(NCluster - 1 downto 0);
    -- Binary value of the valid_ref computed by the Fetch and Decode
    valid_ref_bin  : in  std_logic_vector(ClusterWidth - 1 downto 0);
    -- Offset address to be added to the data ram address
    data_offset    : out std_logic_vector(AddressWidthData - 1 downto 0);
    -- One-hot vector representing which cluster made the match. The '0' corresponds
    -- to the cluste that made the match
    cluster_match  : out std_logic_vector(NCluster - 1 downto 0)
    );
end entity;

architecture behav of AddrGenerator is
  -- Function able to dynamically calculate the right offset
  function dynamicSwitch (
      result        : in std_logic_vector(NCluster - 1 downto 0);
      valid_ref_bin : in std_logic_vector(ClusterWidth - 1 downto 0)
      )
      return std_logic_vector is
  begin
      for i in 0 to NCluster - 1 loop
          if result(i) = '1' then
              return std_logic_vector(to_unsigned(to_integer(unsigned(valid_ref_bin)), AddressWidthData));
          end if;
      end loop;
      return std_logic_vector(to_unsigned(NCluster, AddressWidthData));
  end function;

begin
    data_offset <= dynamicSwitch(cluster_result, valid_ref_bin);

    cluster_match_gen : process (cluster_result)
        variable found_1 : std_logic := '0';
    begin
        for i in 1 to NCluster loop
            if found_1 = '0' then
                cluster_match(NCluster - i) <= not cluster_result(NCluster - i);
                if cluster_result(NCluster - i) = '1' then
                    found_1 := '1';
                end if ;
            else 
                cluster_match(NCluster - i) <= '1'; 
            end if;
        end loop;
        found_1 := '0';
    end process cluster_match_gen;
end behav;
