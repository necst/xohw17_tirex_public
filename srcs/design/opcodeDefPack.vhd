----------------------------------------------------------------------------------
-- Company: Politecnico di Milano
-- Student: Alessandro Comodi
-- Student: Davide Conficconi
-- 
-- Create Date: 03/21/2017 03:56:02 PM
-- Design Name: TiReX - single core
-- Module Name: OpcodePack - package
-- Project Name: TiReX
-- Target Devices: Virtex 7 series
-- Tool Versions: Vivado 16.4
-- Description: Package containing the definition of the OpCodes
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use work.GenericsPack.all;

package OpcodePack is

	-- opcodes in natural form/ positions in the decoder bus
	constant op_or	    : natural := 0; -- OR operator
	constant op_and     : natural := 1; -- AND operator
	constant op_nop     : natural := 2; -- no operation
    constant op_cp_plus : natural := 3; -- )+
    constant op_cp_star : natural := 4; -- )*
    constant op_cp_or	: natural := 5; -- )| 
    constant op_opar	: natural := 6; -- (
    constant op_cp 		: natural := 7; -- )
    constant op_alws    : natural := 8;	-- .
    constant op_jmp		: natural := 9; --jump offset encoding


	-- format of instructions
	--
	-- +---+-----+-------+
	-- ¦ 5 ¦ 4 3 ¦ 2 1 0 ¦
	-- +---+-----+-------+
	-- ¦ ( ¦    |¦ )|    ¦
	-- ¦   ¦ &   ¦ )*    ¦
	-- ¦   ¦     ¦ )+    ¦
	-- ¦   ¦     ¦ )     ¦
	-- |   | . . |       |
	-- +---+-----+-------+

	--opcodes position in instruction
	constant opParpos 	: natural := 5; --open parenthesis (5)
	constant opIpos		: natural := 4; -- and/or/. opcodes (4..3)
	constant opEpos 	: natural := 2;	-- closed parenthesis opcodes(2..0)
	
	--internal/external operators bitwise representation
	constant opIWidth 	: natural := 2; -- bits to represent the internal operators
	constant opEWidth 	: natural := 3; -- bits to represent the internal operators
	constant opParWidth : natural := 1; -- open parenthesis operator

	-- opcodes in binary form

	--internal
	constant opc_or		: std_logic_vector(opIWidth - 1 downto 0) := "01";
	constant opc_and	: std_logic_vector(opIWidth - 1 downto 0) := "10";
	constant opc_alws   : std_logic_vector(opIWidth - 1 downto 0) := "11";

	-- external
	constant opc_cp_plus : std_logic_vector(opEWidth - 1 downto 0) := "010";
	constant opc_cp_star : std_logic_vector(opEWidth - 1 downto 0) := "001";
	constant opc_cp_or	 : std_logic_vector(opEWidth - 1 downto 0) := "011";
	constant opc_cp 	 : std_logic_vector(opEWidth - 1 downto 0) := "100";

	constant opc_jmp 	 : std_logic_vector(opEWidth - 1 downto 0) := "111";

	--open parenthesis
	constant opc_opar    : std_logic := '1';

	--nop
	constant opc_nop	: std_logic_vector(OpCodeWidth - 1 downto 0) := (others => '0');
end OpcodePack;

package body OpcodePack is
end OpcodePack;
