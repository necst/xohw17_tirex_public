----------------------------------------------------------------------------------
-- Company: Politecnico di Milano
-- Student: Alessandro Comodi
-- Student: Davide Conficconi
-- 
-- Create Date: 03/21/2017 03:56:02 PM
-- Design Name: TiReX - single core
-- Module Name: Execute - struct
-- Project Name: TiReX
-- Target Devices: Virtex 7 series
-- Tool Versions: Vivado 16.4
-- Description: This module constitutes the Execute stage of the pipeline.
--				It comprehends the clusters and the engine and some registers
--				and modules that have to treat the data that arrives at the execute
--				stage
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use work.typesPack.all;
use work.OpcodePack.all;

entity Execute is
	generic(
		InternalOpBus		: positive := 2;
		ClusterWidth		: positive := 4;
		NCluster			: positive := 4;
		DataWidth			: positive := 8;
		AddressWidthData	: positive := 6;
		BusWidthData		: positive := 32
		);
	port(
		-- clock signal
		clk					: in std_logic;
		-- reset signal, active LOW
		rst 				: in std_logic;
		-- Signal to indicate a stall in the pipeline
		stall				: in std_logic;
		-- if HIGH comparators are disabled, if LOW they are enabled
		en_comparators		: in std_logic_vector(NCluster - 1 downto 0);
		-- Data coming from the Data RAM
		data 				: in std_logic_vector(BusWidthData - 1 downto 0);
		-- Data contained in the instruction that has been decoded
		reference			: in std_logic_vector(ClusterWidth * DataWidth - 1 downto 0);
		-- Vector that indicates if each data in the reference is valid or not
		valid_ref			: in std_logic_vector(ClusterWidth - 1 downto 0);
		-- operators coming from the Fetch and Decode (only AND and OR)
		operator 			: in std_logic_vector(InternalOpBus - 1 downto 0);
		-- Binary value of the valid_ref signal
		vr_bin				: out std_logic_vector(ClusterWidth - 1 downto 0);
		-- signal to indicate if there was a match among all the clusters
		match				: out std_logic;
		-- One-hot vector to indicate which cluster has made the match
		cluster_match		: out std_logic_vector(NCluster - 1 downto 0);
		-- Jump of the address of the Data Ram to retrieve the next batch of data
		data_offset			: out std_logic_vector(AddressWidthData - 1 downto 0)
		);
end Execute;

architecture struct of Execute is
	
	-- Internal signals to connect the various components
	signal data_buffer_in	: ClusterArray(0 to BusWidthData / DataWidth - 1) := (others => (others => '0'));
	signal data_buffer_out	: ClusterArray(0 to BusWidthData / DataWidth - 1) := (others => (others => '0'));
	signal reference_i		: ClusterArray(0 to ClusterWidth - 1);
	signal results_i 		: std_logic_vector(ClusterWidth * NCluster - 1 downto 0);
begin
	
	VEC_ARR_REF		: entity work.VectArray
		generic map(
			VectWidth		=> DataWidth * ClusterWidth,
			ArrWidth		=> ClusterWidth,
			ArrCellWidth	=> DataWidth
			)
		port map(
			vector_in			=> reference,
			array_out			=> reference_i
			);

	VEC_ARR_REG		: entity work.VectArray
		generic map(
			VectWidth		=> BusWidthData,
			ArrWidth		=> BusWidthData / DataWidth,
			ArrCellWidth	=> DataWidth
			)
		port map(
			vector_in			=> data,
			array_out			=> data_buffer_in
			);

	ENGINE 			: entity work.Engine
		generic map(
			InternalOpBus		=> InternalOpBus,
			NCluster			=> NCluster,
			ClusterWidth		=> ClusterWidth,
			AddressWidthData	=> AddressWidthData
			)
		port map(
			compare_results 	=> results_i,
			valid_ref			=> valid_ref,
			operator			=> operator,
			address_offset		=> data_offset,
			match				=> match,
			vr_bin 				=> vr_bin,
			cluster_match		=> cluster_match,
			stall				=> stall
			);

	generate_clusters	: for i in 0 to NCluster - 1 generate 
		CLUSTER_MUX : entity work.ClusterMux
			generic map(
				ClusterWidth	=> ClusterWidth,
				DataWidth 		=> DataWidth
				)
			port map(
				opcode_is_or	=> operator(op_or),
				en 				=> en_comparators(NCluster - (i + 1)),
				data 			=> data_buffer_in(i to ClusterWidth + (i - 1)),
				reference 		=> reference_i,
				result 			=> results_i((NCluster - i) * ClusterWidth - 1 downto 
												(NCluster - i - 1) * ClusterWidth)
				);
	end generate generate_clusters;
end struct;
