----------------------------------------------------------------------------------
-- Company: Politecnico di Milano
-- Student: Alessandro Comodi
-- Student: Davide Conficconi
-- 
-- Create Date: 03/21/2017 03:56:02 PM
-- Design Name: TiReX - single core
-- Module Name: ControlPath - behav
-- Project Name: TiReX
-- Target Devices: Virtex 7 series
-- Tool Versions: Vivado 16.4
-- Description: This component is the controller of the Core, it fetches the
--			    right data and instructions, it monitors the matching or not 
--				matching phase and check the validity of the results obtained
--				in the exectuion, it is a Finite State Machine that coordinates
--				all the components in the Tile and keep track of the status of
--				data and regular expression matching.
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
----------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use ieee.numeric_std.all;
use work.OpcodePack.all;
use work.StackPack.all;

entity ControlPath is
	generic(

		AddrWidthInstr		: positive := 4;
		AddrWidthData		: positive := 6;
		OpBusWidth			: positive := 10;
		CharacterNumber		: positive := 11;
		ClusterWidth		: positive := 4;
		InternalOpBus		: positive := 2;
		CounterWidth		: positive := 3;

	    -- StackDataWidth    : width of each element of the buffer contained in the stack
	    StackDataWidth     : positive := 24;
	    -- BufferAddressWidth: with of the address space of the buffer inside of
	    -- the stack (2^BufferAddressWidth is the number of the elements in the stack)
	    BufferAddressWidth : positive := 3;

	    DataWidth 			: positive := 8;
		NCluster			: positive := 4
		);
	port(
		-- External signal

		clk					: in std_logic;
		-- reset active low
		rst					: in std_logic;
		--enable the search of the reg exp
		src_en				: in std_logic;
		--the address starting point of the instruction
		addr_start_instr	: in std_logic_vector(AddrWidthInstr - 1 downto 0);	
		-- 
		first_character		: in std_logic_vector(CharacterNumber - 1 downto 0);
		ending_character	: in std_logic_vector(CharacterNumber - 1 downto 0);
		--
		-- 1 if a match is found
		found				: out std_logic;
		--end of search, if also found is high a match is found
		complete			: out std_logic;
		-- complete: bitmap [2 1 0] --> 0 = end of data
		--								1 = always match
		--								2 = NOP found in match state 
		where_complete		: out std_logic_vector(2 downto 0);

		-- Control signal
		
		--
		curr_character	 	: out std_logic_vector(CharacterNumber - 1 downto 0);
		curr_last_match_char: out std_logic_vector(CharacterNumber - 1 downto 0);
		reload_bram			: out std_logic_vector(1 downto 0) := "00";
		reload_complete		: in std_logic;
		--

		--input offset signal coming from the FD stage whenever a jump is the current instruction
		jump_offset			: in std_logic_vector(ClusterWidth * DataWidth - 1 downto 0);
		-- input  signal that comes whenever a open parenthesis to understand which kind of computation we expect
		jump_flag 			: in std_logic;

		-- address of the first instruction in the instruction memory
		addr_instruction_1	: out std_logic_vector(AddrWidthInstr - 1 downto 0);
		-- address of the second instruction in the instruction memory
		addr_instruction_2	: out std_logic_vector(AddrWidthInstr - 1 downto 0);
		--address of special instruction in order to avoid loss of clock cycles
		addr_instruction_3	: out std_logic_vector(AddrWidthInstr - 1 downto 0);
		-- address of the data in the buffer data memory
		addr_data 			: out std_logic_vector(AddrWidthData - 1 downto 0);
		-- 1 is to stop the execute stage
		stall 				: out std_logic;
		-- "00" stands for the FD_A "01" for the FD_B, "10" and "11" for the FD_C
		sel_fd				: out std_logic_vector(1 downto 0);
		-- signal of clusters that matches
		cluster_match		: in std_logic_vector(NCluster - 1 downto 0);

		-- Internal signal

		--op code from fd stage
		op_code				: in std_logic_vector(OpBusWidth - 1 downto 0);
		--a match is found in the current execution stage
		match				: in std_logic;
		-- offset for the new data address
		data_offset 		: in std_logic_vector(AddrWidthData - 1 downto 0);
		-- offset for the new data address
		enable_comparators	: out std_logic_vector(NCluster - 1 downto 0)
		);
end ControlPath;

architecture behav of ControlPath is

	
	---Internal signals

	-- accumulator of the matching, needed to spot a match in nop stage
	signal match_acc 				: std_logic := '0';
	-- address of when the match is started
	signal last_match				: std_logic_vector(AddrWidthData - 1 downto 0);
	 -- save the data offset for the correct offset of the last match
	signal data_offset_reg			: std_logic_vector(AddrWidthData - 1 downto 0);
	-- starting opcode of the reg exp, or first opcode of the nested level
	signal op_code_reg				: std_logic_vector(OpBusWidth - 1 downto 0) := (others => '0');
	-- save the enable comparators signal
	signal enable_comparators_i 	: std_logic_vector(NCluster - 1 downto 0);

	--Stack related signals
	
	signal rst_stack_in 			: std_logic :='1';
	signal rst_stack_ctr 			: std_logic :='1';
	signal stack_we					: std_logic;
	signal stack_din				: std_logic_vector(StackDataWidth - 1 downto 0);
	signal stack_dout				: std_logic_vector(StackDataWidth - 1 downto 0);
	signal stack_empty 				: std_logic;
	signal stack_full				: std_logic;
	signal stack_pop				: std_logic;

	signal stack_ram				: std_logic;
	signal stack_instr				: std_logic;

	--FSM related signals
	type STATE_TYPE is (RESET, NOP, FD, EX_NM, EX_M, STL_BRAM);
	signal state : STATE_TYPE := RESET;

	--I/O signals
	-- address of the next data
	signal ram_addr 				: std_logic_vector(AddrWidthData - 1 downto 0);
	-- address of the first instruction
	signal addr_instruction_1_reg 	: std_logic_vector(AddrWidthInstr - 1 downto 0) := (others => '0');
	-- addres of the current matching instruction
	signal addr_instruction_2_reg 	: std_logic_vector(AddrWidthInstr - 1 downto 0) := (others => '0');
	-- address of the special instruction
	signal addr_instruction_3_reg	: std_logic_vector(AddrWidthInstr - 1 downto 0) := (others => '0');
	-- internal signal for the complete
	signal complete_i 				: std_logic;
	-- internal signal for the stall, needed to be sequential the stall
	signal stall_i					: std_logic := '0';
	
	-- Already defined in stackDefpack, but needed in order to have customizable IP
	--constant CounterWidth 			: natural := 3;
	constant counterMSB 			: natural := StackDataWidth - 1;
	constant counterLSB				: natural := StackDataWidth - CounterWidth;
	constant op_codeMSB				: natural := counterLSB - 1;
	constant op_codeLSB				: natural := counterLSB - OpBusWidth;
	constant matchAccum				: natural := op_codeLSB - 1;
	constant specialaddrMSB			: natural := matchAccum - 1;
	constant specialaddrLSB			: natural := matchAccum - AddrWidthInstr;
	constant contextaddrMSB			: natural := specialaddrLSB - 1;
	constant contextaddrLSB			: natural := specialaddrLSB - AddrWidthData;

	signal kleene_counter			: std_logic_vector(CounterWidth - 1 downto 0) := (others => '0');
	signal where_complete_i			: std_logic_vector(2 downto 0);

	signal jump_offset_i			: std_logic_vector(ClusterWidth * DataWidth - 1 downto 0) := (others => '0');

	signal flag_special_init		: std_logic := '0';
	signal data_offset_last_match	: std_logic_vector(AddrWidthData - 1 downto 0) := (others => '0');
	signal not_match				: std_logic := '0';

	-- Future signal for nested parenthesis
	--signal nested					: std_logic := '0';

	signal curr_character_i			: std_logic_vector(CharacterNumber - 1 downto 0);
	signal curr_last_match_char_i		: std_logic_vector(CharacterNumber - 1 downto 0);
	signal curr_character_reg		: std_logic_vector(CharacterNumber - 1 downto 0) := (others => '0');
	signal reload_i					: std_logic := '0';
	constant char_padding			: std_logic_vector(CharacterNumber - AddrWidthData - 1 downto 0) := (others => '0');
	constant ram_addr_padding		: std_logic_vector(AddrWidthData - 1 downto 4) := (others => '0');
begin


	STACK_BUFFER : entity work.StackBuffer
		generic map(
			StackDataWidth 		=> StackDataWidth,
			BufferAddressWidth 	=> BufferAddressWidth
			)
		port map (
				clk 		=> clk,
				rst 		=> rst_stack_in,
				we 			=> stack_we,
				pop 		=> stack_pop,
				empty 		=> stack_empty,
				full 		=> stack_full,
				data_in 	=> stack_din,
				data_out 	=> stack_dout
			);
	-- Register and FSM
	Rst_FSM_proc : process(clk, rst)
	begin
	-- Asynchronous reset active low
	if rst = '0' then
		state <= RESET;
		-- comparators disabled
		enable_comparators 		<= (others => '1');
		match_acc	 			<= '0';
		complete_i 				<= '0';
		found 					<= '0';
		stall_i 				<= '0';
		ram_addr 				<= (others => '0');

		kleene_counter			<= (others => '0');
		rst_stack_ctr 			<= '1';
		stack_we 				<= '0';
		stack_pop 				<= '0';
		stack_instr				<= '0';
		stack_ram				<= '0';
		stack_din				<= (others => '0');
		curr_character_i		<= (others => '0');
		curr_last_match_char_i	<= (others => '0');
		curr_character_reg		<= (others => '0');
	-------------------- FSM sequantially active --------------------
	elsif rising_edge(clk) then
		case( state ) is
		-------------------- Reset stage --------------------
			when RESET 	=>
				state 					<= NOP;
				enable_comparators 		<= (others => '1');
				complete_i 				<= '0';
				match_acc 				<= '0';
				found					<= '0';
				sel_fd 					<= "00";
				--already active external reset
				rst_stack_ctr 			<= '1'; 
				stack_pop				<= '0';

				flag_special_init		<= '0';
				data_offset_last_match 	<= (others => '0');
				jump_offset_i 			<= (others => '0');
				not_match				<= '0';

			-------------------- NOP stage --------------------	
			when NOP 	=>
				stack_we           <= '0';
          		stack_pop          <= '0';

				-- Comparators disabled
				enable_comparators 	<= (others => '1');
				found <= match_acc;
				if src_en = '0' or complete_i = '1' then
					state <= NOP;
				else
					-- fetch instructions
					addr_instruction_1_reg 	<= addr_start_instr;
					addr_instruction_2_reg 	<= addr_start_instr + 1;
					addr_instruction_3_reg	<= addr_start_instr	+ 2;
					-- fetch data
					ram_addr 				<= (others => '0');
					--reset address
					last_match 				<= (others => '0');
					--reset bram track
					curr_last_match_char_i	<= first_character;
					curr_character_i		<= first_character;
					state 					<= FD;
				end if ;

			-------------------- Fetch and Decode stage --------------------
			when FD 	=>

				stack_we           <= '0';
          		stack_pop          <= '0';
				sel_fd <= "00";
				-- comparators enabled
				enable_comparators <= (others => '0');
				state <= EX_NM;


			------------ Execution stage, currently not matching-----------

			-- In execution: in the previous state there was no match
			when EX_NM 	=>
				rst_stack_ctr 	   <= '1';
				stack_we           <= '0';
          		stack_pop          <= '0';
          		stall_i 		   <= '0';
			 	
			 	-- normal behavior of the core
				sel_fd 				<= "00";
				data_offset_reg 	<= data_offset;
				data_offset_last_match <= (others => '0');
				
				curr_character_reg	<= curr_character_i;
				-- comparator endabled
				enable_comparators 	<= (others => '0');
				-- End of data need to exit
				if curr_character_i >= ending_character then
						complete_i 			<= '1';
						match_acc 			<= '0';
						state 				<= NOP;
						where_complete_i	<= "001";

				elsif stall_i = '0' then 

					if op_code(op_alws) = '1' then
						complete_i			<= '1';
						match_acc 			<= '1';
						state 				<= NOP;
						where_complete_i	<= "010";
					elsif op_code(op_nop) = '1' then 
						-- regexp completed 
						match_acc 	<= '0';
						state 		<= NOP;

					elsif op_code(op_opar) = '1' then
					-- start my reg exp with an open parenthesis
						sel_fd 										<= "01";
						last_match 									<= ram_addr;
						curr_last_match_char_i						<= curr_character_i;
						match_acc 									<= '0';
						flag_special_init							<= '1';
						not_match									<= '1';
						ram_addr 									<= ram_addr;
						--if i'm starting with a jump I have to prefetch the jumped instruction 
						if jump_flag = '1' then
							addr_instruction_2_reg						<= addr_instruction_2_reg + 1;
							addr_instruction_3_reg						<= addr_start_instr + jump_offset_i(AddrWidthInstr - 1 downto 0);
						else
							op_code_reg 								<= op_code;
							addr_instruction_2_reg					 	<= addr_instruction_2_reg + 1;
							addr_instruction_3_reg						<= addr_instruction_2_reg;
						end if ;
						
						enable_comparators 							<= (others => '0');
						enable_comparators_i 						<= (others => '0');
						stack_we 									<= '1';
						stack_din 									<= kleene_counter & op_code_reg & match_acc & addr_instruction_3_reg & ram_addr;

						state <= EX_M;
					elsif op_code(op_jmp) = '1' then
						op_code_reg 			<= op_code;
						jump_offset_i 			<= jump_offset;
						ram_addr 				<= ram_addr;
						sel_fd 					<= "01";
						curr_character_i 		<= curr_character_i;
						addr_instruction_2_reg 	<= addr_instruction_2_reg + 1;
						match_acc <= '0';
						state <= EX_NM;

						
					elsif match = '1' then -- start the point of matching
						op_code_reg 		 	<= op_code;
						-- Store the starting point of matching
						last_match 			 	<= ram_addr;
						data_offset_last_match 	<= data_offset;
						-- use the prefetcher
						sel_fd 				 	<= "01";
						-- Fetch new instruction and data
						addr_instruction_2_reg  <= addr_instruction_2_reg + 1;
						ram_addr 			 	<= ram_addr + data_offset;
						match_acc 			 	<= '1';
						-- maintain active only the clusters that match
						enable_comparators_i 	<= cluster_match;
						enable_comparators 		<= cluster_match;

						curr_character_i 		<= curr_character_i + (char_padding & data_offset);
						curr_last_match_char_i	<= curr_character_reg;
						reload_bram(1)		 	<= '1';

						state <= EX_M;
					else
					-- matching nothing

						sel_fd					<= "00";
						addr_instruction_1_reg 	<= addr_start_instr;
						addr_instruction_2_reg 	<= addr_start_instr + 1;
						addr_instruction_3_reg	<= addr_start_instr + 2;
					 	match_acc 				<= '0';
					 	ram_addr 				<= ram_addr + data_offset;
					 	reload_bram(1)			<= '0';
				 		curr_character_i 		<= curr_character_i + (char_padding & data_offset);
					 	state 					<= EX_NM;
					end if;
	
				end if ;


			------------ Execution stage, currently matching-----------

			-- Execution and in the previous state a match was found
			when EX_M 	=>
				data_offset_reg 	<= data_offset;
				enable_comparators 	<= enable_comparators_i;
				stack_we			<= '0';
				stack_pop			<= '0';
				rst_stack_ctr		<= '1';
				stack_ram			<= '0';
				stack_instr			<= '0';

				--resetting ram addr
				if stack_ram = '1' then
					ram_addr <= stack_dout(contextAddrMSB downto contextAddrLSB);					
				end if ;
				--resetting instr
				if stack_instr = '1' then
					addr_instruction_3_reg <= stack_dout(specialaddrMSB downto specialaddrLSB);
				end if ;

				reload_bram(1)		<= '0';
				--special condition when start with open par, to check when we consider nested parenthesis
				if flag_special_init = '1' AND ( op_code(op_or) = '1' OR op_code(op_and) = '1' )then 
					last_match 				<= ram_addr;
					curr_last_match_char_i 	<= curr_character_i;
					data_offset_last_match 	<= data_offset;
					flag_special_init 		<= '0';
				end if ;
				-- end of the valid instructions/reg exp
				if op_code(op_nop) = '1' then
					complete_i			<= '1';
					state 				<= NOP;
					where_complete_i	<= "100";
				--currently not implemented the "." operator
				elsif op_code(op_alws) = '1' then
					complete_i 			<= '1';
					match_acc 			<= '1';
					state 				<= NOP;
					where_complete_i 	<= "010";
				-- end of data exit
				elsif curr_character_i >= ending_character then
					complete_i 			<= '1';
					match_acc 			<= '0';
					state 				<= NOP;
					where_complete_i	<= "001";
				-- open parenthesis operator
				elsif op_code(op_opar) = '1' then
					sel_fd 					<= "01";
					op_code_reg				<= op_code;
					ram_addr 				<= ram_addr;
					-- jump inside the match
					if jump_flag = '1' then 
						addr_instruction_2_reg	<= addr_instruction_2_reg + 1;
						addr_instruction_3_reg	<= addr_start_instr + jump_offset_i(AddrWidthInstr - 1 downto 0);
					else
						addr_instruction_2_reg 	<= addr_instruction_2_reg + 1;
						addr_instruction_3_reg	<= addr_instruction_2_reg;
					end if ;
					stack_we 				<= '1';
					stack_din 				<= kleene_counter & op_code_reg & match_acc & addr_instruction_3_reg & ram_addr;
					state 					<= EX_M;
				-- closed parenthesis operator
				elsif op_code(op_cp) = '1' then
					if match = '1' then
						sel_fd 					<="01";
						match_acc 				<= '1';
						enable_comparators 		<= cluster_match;
						enable_comparators_i	<= cluster_match;
						ram_addr 				<= ram_addr + data_offset;
						stack_pop 				<= '1';
						if not_match = '1' then
							not_match <= '0';
							enable_comparators_i <= cluster_match;
							enable_comparators 	 <= cluster_match;
						end if ;
						-- TODO : Check the nesting with the stack
						--addr_instruction_3_reg 	<= stack_dout( specialaddrMSB downto specialaddrLSB);
						stack_instr 			<= '1';
						addr_instruction_2_reg 	<= addr_instruction_2_reg + 1;
						not_match				<= '0';
						rst_stack_ctr			<= '0';
						state 					<= EX_M;
					else
						if not_match = '1' then
							if op_code_reg(op_opar) = '1' then
								curr_character_i		<= curr_last_match_char_i + (char_padding & data_offset);
								curr_character_reg		<= curr_last_match_char_i + (char_padding & data_offset);
								ram_addr 				<= last_match + data_offset;
							else
								curr_character_i		<= curr_last_match_char_i + (char_padding & data_offset_last_match);
								curr_character_reg		<= curr_last_match_char_i + (char_padding & data_offset_last_match);
								ram_addr 				<= last_match + data_offset_last_match;
							end if;
						else
							curr_character_i		<= curr_last_match_char_i + 1;
							curr_character_reg		<= curr_last_match_char_i + 1;
							ram_addr 				<= last_match + 1;
						end if ;
						match_acc 				<= '0';
						rst_stack_ctr 			<= '0';
						sel_fd 					<= "00";
						addr_instruction_2_reg 	<= addr_start_instr + 1;
						addr_instruction_3_reg	<= addr_start_instr + 2;
						state 					<= EX_NM;
						if reload_i = '1' then
							enable_comparators  <= (others => '1');
							stall_i				<= '1';
							state 				<= STL_BRAM;
						else
							state 				<= EX_NM;
						end if;
					end if ;
				-- )+ operator, at least one repetition of what is inside parenthesis
				elsif op_code(op_cp_plus) = '1' then
					-- still matching my repetition
					if match = '1' then
						sel_fd 					<= "10";
						stack_we 				<= '1';
						stack_din 				<= kleene_counter & op_code_reg & match_acc & addr_instruction_3_reg & ram_addr;
						addr_instruction_2_reg 	<= addr_instruction_3_reg + 1;
						kleene_counter 			<= kleene_counter + 1;
						match_acc 				<= '1';
						ram_addr 				<= ram_addr + data_offset;
						curr_character_i		<= curr_character_i + (char_padding & data_offset);
						if not_match = '1' then
							not_match <= '0';
							enable_comparators_i <= cluster_match;
							enable_comparators 	 <= cluster_match;
						end if ;
						state 					<= EX_M;
					else
					-- if not match what is inside parenthesis check if at least one repetition
					-- otherwise the is a failure in the matching
						if kleene_counter > 0 then
							sel_fd 					<= "01";
							addr_instruction_2_reg 	<= addr_instruction_2_reg + 1;
							--TODO check stack for nesting purpose
							--addr_instruction_3_reg 	<= stack_dout(specialaddrMSB downto specialaddrLSB);
							stack_instr				<= '1';
							-- need to pop also opcode reg for nesting purpose
							match_acc 				<= '1';
							ram_addr 				<= ram_addr;
							curr_character_i		<= curr_character_i;
							kleene_counter			<= (others => '0');
							state 					<= EX_M;
						else
							sel_fd 					<= "00";
							addr_instruction_2_reg 	<= addr_start_instr + 1;
							match_acc 				<= '0';
							enable_comparators 		<= (others => '0');
							ram_addr 				<= last_match + 1;
							
							curr_character_i		<= curr_last_match_char_i + 1;
							curr_character_reg		<= curr_last_match_char_i + 1;

							kleene_counter 			<= (others => '0');
							rst_stack_ctr 			<= '0';
							
							if reload_i = '1' then
								enable_comparators  <= (others => '1');
								stall_i				<= '1';
								state 				<= STL_BRAM;
							else
								state 				<= EX_NM;
							end if;
						end if ;
								
					end if ;
				-- )* operator, the same as above but in this case don't have a minimum number of repetition
				elsif op_code(op_cp_star) = '1' then
						if not_match = '1' then
							not_match <= '0';
							enable_comparators_i <= cluster_match;
							enable_comparators 	 <= cluster_match;
						end if ;
					if match = '1' then
						sel_fd 					<= "10";
						stack_we 				<= '1';
						stack_din 				<= kleene_counter & op_code_reg & match_acc & addr_instruction_3_reg & ram_addr;
						addr_instruction_2_reg 	<= addr_instruction_3_reg + 1;
						kleene_counter 			<= kleene_counter + 1;
						match_acc 				<= '1';
						ram_addr 				<= ram_addr + data_offset;
						curr_character_i		<= curr_character_i + (char_padding & data_offset);
						
						state 					<= EX_M;
					else
						sel_fd 					<= "01";
						rst_stack_ctr 			<= '0';
						addr_instruction_2_reg 	<= addr_instruction_2_reg + 1;
						stack_instr <= '1';
						--addr_instruction_3_reg 	<= stack_dout(specialaddrMSB downto specialaddrLSB);
						-- TODO stack
						ram_addr 				<= ram_addr;
						curr_character_i		<= curr_character_i;
						kleene_counter 			<= (others => '0');
						match_acc 				<= '1';
						
						state 					<= EX_M;							
					end if ;	
				-- )| I'm searching for more patterns at the same time, but if I found one can go forward
				-- to others patterns
				elsif op_code(op_cp_or) = '1' then
					if match = '1' then
						sel_fd 					<= "10";
						ram_addr 				<= ram_addr + data_offset;
						curr_character_i		<= curr_character_i + (char_padding & data_offset);
						curr_character_reg		<= curr_character_i + (char_padding & data_offset);
						addr_instruction_2_reg 	<= addr_instruction_3_reg + 1;
						enable_comparators 		<= cluster_match;
						enable_comparators_i	<= cluster_match;
						match_acc 				<= '1';
						rst_stack_ctr 			<= '0';
						state 					<= EX_M;
					else
							sel_fd 					<= "01";
							stack_pop				<= '1';
							stack_ram				<= '1';
							curr_character_i		<= curr_last_match_char_i;
							curr_character_reg		<= curr_last_match_char_i;
							addr_instruction_2_reg 	<= addr_instruction_2_reg + 1;
							match_acc 				<= '0';
							state 					<= EX_M;
					end if ;
				-- found a jump instruction inside my reg exp this tell me how to instruct the third F&D stage
				elsif op_code(op_jmp) = '1' then
						jump_offset_i 			<= jump_offset;
						ram_addr 				<= ram_addr;
						sel_fd 					<= "01";
						curr_character_i 		<= curr_character_i;
						
						addr_instruction_2_reg 	<= addr_instruction_2_reg + 1;
						addr_instruction_3_reg 	<= addr_start_instr + jump_offset(AddrWidthInstr - 1 downto 0);
						match_acc <= '0';
						state <= EX_M;
					
				-- match found	
				elsif match = '1' then
						sel_fd 					<= "01";
						match_acc 				<= '1';
						addr_instruction_2_reg 	<= addr_instruction_2_reg + 1;
						ram_addr 				<= ram_addr + data_offset;
						curr_character_i 		<= curr_character_i + (char_padding & data_offset);
						state <= EX_M;

				else -- not matching rollback
					sel_fd <= "00";

					if match_acc = '0' then --distinguish starting case with open parenthisis
						if not_match = '1' then
							not_match <= '0';
							enable_comparators_i <= cluster_match;
							enable_comparators 	 <= cluster_match;
						end if ;
						ram_addr <= ram_addr + data_offset;
						curr_character_i 		<= curr_character_i + (char_padding & data_offset);
						curr_character_reg 		<= curr_character_i + (char_padding & data_offset);
						rst_stack_ctr 			<= '0';
						reload_bram(1)			<= '0';
					else -- normal not matching
						ram_addr 				<= last_match + 1;
						curr_character_i		<= curr_last_match_char_i + 1;
						curr_character_reg		<= curr_last_match_char_i + 1;
						
					end if ;
					addr_instruction_2_reg 	<= addr_start_instr + 1;
					match_acc 				<= '0';
					enable_comparators 		<= (others => '0');
					if reload_i = '1' then
						enable_comparators  <= (others => '1');
						stall_i				<= '1';
						state 				<= STL_BRAM;
					else
						state 				<= EX_NM;
					end if;
				end if;


			------------ Stall needed for BRAM-----------	
			-- state for BRAM stall, need to wait if I don't have my data
			when STL_BRAM =>
				if reload_complete = '0' then
					enable_comparators 	<= (others => '0');
					stall_i				<= '0';
					state 				<= EX_NM;
				else
					state <= STL_BRAM;
				end if;


			------------ Failure state-----------
			-- Something goes wrong
			when others =>
				sel_fd 				<= "00";
				stall_i 			<= '1';
				enable_comparators 	<= (others => '1');
				
				state 				<= RESET;
		end case ;
	end if;
	end process;

	last_match_overwritten : process(clk)
	begin
		if state = EX_M and match = '0' and curr_character_i - curr_last_match_char_i >= 
				std_logic_vector(to_unsigned((2**AddrWidthData), CharacterNumber)) then
			reload_bram(0) 	<= '1';
			reload_i		<= '1';
		else 
			reload_bram(0) 	<= '0';
			reload_i		<= '0';
		end if;
	end process last_match_overwritten;

	--next state logic
	curr_character 		<= curr_character_i;
	curr_last_match_char<= curr_last_match_char_i;
	stall 				<= stall_i;
	addr_instruction_1 	<= addr_instruction_1_reg;
	addr_instruction_2 	<= addr_instruction_2_reg;
	complete 			<= complete_i;
	where_complete 		<= where_complete_i;

	--combinatorial part
	rst_stack_in 		<= rst and rst_stack_ctr;

	--mixed
	addr_data 	 		<= stack_dout(contextAddrMSB downto contextAddrLSB) when stack_ram = '1'
						else	ram_addr;
	addr_instruction_3 	<= stack_dout(specialaddrMSB downto specialaddrLSB) when stack_instr = '1'
						else addr_instruction_3_reg;
end behav;