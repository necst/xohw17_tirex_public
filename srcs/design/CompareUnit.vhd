----------------------------------------------------------------------------------
-- Company: Politecnico di Milano
-- Student: Alessandro Comodi
-- Student: Davide Conficconi
-- 
-- Create Date: 03/21/2017 03:56:02 PM
-- Design Name: TiReX - single core
-- Module Name: CompareUnit - behav
-- Project Name: TiReX
-- Target Devices: Virtex 7 series
-- Tool Versions: Vivado 16.4
-- Description: This module is the simplest single comparator. It does the
--				Between the reference and the data and prouces a result.
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity CompareUnit is
	generic(
		DataWidth		: positive := 8
		);
	port(
		-- enable comparators signal. if HIGH comparators are disabled, if LOW
		-- the comparator is enabled
		en 				: in std_logic;
		-- data to be checked
		data 			: in std_logic_vector(DataWidth - 1 downto 0);
		-- reference to be put in comparison with the data
		reference		: in std_logic_vector(DataWidth - 1 downto 0);
		-- result of the comparison
		result			: out std_logic
		);
end CompareUnit;

architecture behav of CompareUnit is

begin
	process(en, data, reference)
	begin
		if en = '1' then
			result <= '0';

		else
			if data = reference then
				result <= '1';
			else
				result <= '0';
			end if;
		end if;
	end process;
end behav;
