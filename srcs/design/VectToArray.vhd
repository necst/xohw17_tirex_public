----------------------------------------------------------------------------------
-- Company: Politecnico di Milano
-- Student: Alessandro Comodi
-- Student: Davide Conficconi
-- 
-- Create Date: 03/21/2017 03:56:02 PM
-- Design Name: TiReX - single core
-- Module Name: VectArray - behav
-- Project Name: TiReX
-- Target Devices: Virtex 7 series
-- Tool Versions: Vivado 16.4
-- Description: This module generates an array of vectors. Each cell of the array
--              Contains the data to be given to the comparators. This helps the
--              distribution of the data among all the clusters.
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library ieee;
use ieee.std_logic_1164.all;
use work.typesPack.all;

entity VectArray is

    generic (
        VectWidth    : positive := 16;
        ArrWidth     : positive := 2;
        ArrCellWidth : positive := 8
        );

    port (
        vector_in    : in  std_logic_vector(VectWidth - 1 downto 0);
        array_out    : out RegisterArray(0 to ArrWidth - 1)
        );

end VectArray;

architecture behav of VectArray is
begin
    process (vector_in)
    begin
        for i in 0 to ArrWidth - 1 loop
            array_out(i) <= vector_in((VectWidth - 1 - i * ArrCellWidth) downto
                           (VectWidth - (i + 1) * ArrCellWidth));
        end loop;
    end process;
end behav;