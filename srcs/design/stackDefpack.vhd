----------------------------------------------------------------------------------
-- Company: Politecnico di Milano
-- Student: Alessandro Comodi
-- Student: Davide Conficconi
--
-- Create Date: 06/12/2017 04:19:26 PM
-- Design Name: TiReX - single core
-- Module Name: stackDefpack
-- Module Name: GenericsPack - package
-- Project Name: TiReX
-- Target Devices: Virtex 7 series
-- Tool Versions: Vivado 16.4
-- Description: Predefined constants for Stack Related signals
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use work.GenericsPack.all;

package Stackpack is

--constant StackDataWidth			: natural := 24;

constant counterMSB 			: natural := StackDataWidth - 1;
constant counterLSB				: natural := StackDataWidth - CounterWidth;
constant op_codeMSB				: natural := counterLSB - 1;
constant op_codeLSB				: natural := counterLSB - OpCodeBus;
constant matchAccum				: natural := op_codeLSB - 1;
constant specialaddrMSB			: natural := matchAccum - 1;
constant specialaddrLSB			: natural := matchAccum - AddressWidthInstr;
constant contextaddrMSB			: natural := specialaddrLSB - 1;
constant contextaddrLSB			: natural := specialaddrLSB - AddressWidthData;

end Stackpack;

package body Stackpack is
end Stackpack;
