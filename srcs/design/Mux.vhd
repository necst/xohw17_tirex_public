----------------------------------------------------------------------------------
-- Company: Politecnico di Milano
-- Student: Alessandro Comodi
-- Student: Davide Conficconi
-- 
-- Create Date: 03/21/2017 03:56:02 PM
-- Design Name: TiReX - single core
-- Module Name: Mux - behav
-- Project Name: TiReX
-- Target Devices: Virtex 7 series
-- Tool Versions: Vivado 16.4
-- Description: This is a simple component that based on the selection puts in the
--				out signal one of the two inputs
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;

entity Mux is
	generic(
		DataWidth		: positive
		);
	port(
		data_in_1		: in std_logic_vector(DataWidth - 1 downto 0);
		data_in_2 		: in std_logic_vector(DataWidth - 1 downto 0);
		sel 			: in std_logic;
		data_out		: out std_logic_vector(DataWidth - 1 downto 0)
		);
end Mux;

architecture behav of Mux is
	
begin
	process (data_in_1, data_in_2, sel)
	begin
		if sel = '0' then
			data_out <= data_in_1;
		else 
			data_out <= data_in_2;
		end if;
	end process;
end behav;
