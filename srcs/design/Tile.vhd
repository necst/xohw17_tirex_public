----------------------------------------------------------------------------------
-- Company: Politecnico di Milano
-- Student: Alessandro Comodi
-- Student: Davide Conficconi
--
-- Create Date: 03/30/2017 02:43:30 PM
-- Design Name: TiReX - single core
-- Module Name: Tile - Behavioral
-- Project Name: TiReX
-- Target Devices: Virtex 7 series
-- Tool Versions: Vivado 16.4
-- Description: This is a wrapper that connects RamIntr RamData and the TiRexCore
--              in a Tile
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use work.GenericsPack.all;
use work.OpCodePack.all;

entity Tile is
    generic(
        ---------------Core----------------------
        AddressWidthInstr       : positive := 4;
        AddressWidthData        : positive := 6;
        CharacterNumber         : positive := 14;

        OpCodeBus               : positive := 10;
        OpCodeWidth             : positive := 6;
        InternalOpBus           : positive := 2;
        --
        DataWidth               : positive := 8;
        ClusterWidth            : positive := 4;
        NCluster                : positive := 4;

        CounterWidth            : positive := 3;
        BufferAddressWidth      : positive := 3;
        StackDataWidth          : positive := 24;

        ExternalBusWidthData    : positive := 128;
        InternalBusWidthData    : positive := 56;
        ExternalBusWidthInstr   : positive := 32;
        RamWidthInstr           : positive := 38;
        RamWidthData            : positive := 8
        );

    port ( 
        clk              : in std_logic;
        rst              : in std_logic;
        addr_start_instr : in std_logic_vector(AddressWidthInstr - 1 downto 0);
        
        first_character  : in std_logic_vector(CharacterNumber - 1 downto 0);
        ending_character : in std_logic_vector(CharacterNumber - 1 downto 0);

        curr_character        : out std_logic_vector(CharacterNumber - 1 downto 0);
        curr_last_match_char  : out std_logic_vector(CharacterNumber - 1 downto 0);
        reload_bram           : out std_logic_vector(1 downto 0);
        reload_complete       : in std_logic;

        src_en           : in std_logic;

        complete         : out std_logic;
        found            : out std_logic;
        where_complete   : out std_logic_vector(2 downto 0);

        instr_address_wr : in std_logic_vector(AddressWidthInstr - 1 downto 0);
        instr_data_in    : in std_logic_vector(ExternalBusWidthInstr - 1 downto 0);
        instr_we_data    : in std_logic;
        instr_we_opcode  : in std_logic;

        data_address_wr  : in std_logic_vector(AddressWidthData - 1 downto 0);
        data_data_in     : in std_logic_vector(ExternalBusWidthData - 1 downto 0);
        data_we          : in std_logic
        );
end Tile;

architecture Behavioral of Tile is

    signal ram_instr_addr_rd_1_int      : std_logic_vector (AddressWidthInstr - 1 downto 0);
    signal ram_instr_out_1_int          : std_logic_vector (RamWidthInstr - 1 downto 0);

    signal ram_instr_addr_rd_2_int      : std_logic_vector (AddressWidthInstr - 1 downto 0);
    signal ram_instr_out_2_int          : std_logic_vector (RamWidthInstr - 1 downto 0);


    signal ram_instr_out_3_int          : std_logic_vector (RamWidthInstr - 1 downto 0);
    signal ram_instr_addr_rd_3_int      : std_logic_vector (AddressWidthInstr - 1 downto 0);

    signal ram_data_address_rd_int      : std_logic_vector(AddressWidthData - 1 downto 0);
    signal ram_data_out_int             : std_logic_vector(InternalBusWidthData - 1 downto 0);
begin
  RAMDATA       : entity work.RamData
      generic map(
          RamWidth          => RamWidthData,
          ExternalBusWidth  => ExternalBusWidthData,
          InternalBusWidth  => InternalBusWidthData,
          AddressWidth      => AddressWidthData
        )
      port map(
          rst             => rst,
          clk             => clk,
          address_rd      => ram_data_address_rd_int,
          address_wr      => data_address_wr,
          data_in         => data_data_in,
          we              => data_we,
          data_out        => ram_data_out_int
        );
  RAMINSTRUCTION : entity work.RamInstr
      generic map(
          BusWidth        => ExternalBusWidthInstr,
          DataWidth       => DataWidth,
          OpCodeWidth     => OpCodeWidth,
          ClusterWidth    => ClusterWidth,
          RamWidth        => RamWidthInstr,
          AddressWidth    => AddressWidthInstr
        )
      port map(
          rst             => rst,
          clk             => clk,
          address_rd_1    => ram_instr_addr_rd_1_int,
          address_rd_2    => ram_instr_addr_rd_2_int,
          address_rd_3    => ram_instr_addr_rd_3_int,
          address_wr      => instr_address_wr,
          data_in         => instr_data_in,
          we_data         => instr_we_data,
          we_opcode       => instr_we_opcode,
          data_out_1      => ram_instr_out_1_int,
          data_out_2      => ram_instr_out_2_int,
          data_out_3      => ram_instr_out_3_int
        );
  TiReXCORE      : entity work.TirexCore
      generic map(
          AddressWidthInstr => AddressWidthInstr,
          AddressWidthData  => AddressWidthData,
          CharacterNumber   => CharacterNumber,
          OpCodeBus         => OpCodeBus,
          OpCodeWidth       => OpCodeWidth,
          InternalOpBus     => InternalOpBus,
          DataWidth         => DataWidth,
          ClusterWidth      => ClusterWidth,
          NCluster          => NCluster,
          BusWidthData      => InternalBusWidthData,

          CounterWidth      => CounterWidth,
          BufferAddressWidth => BufferAddressWidth,
          StackDataWidth    => StackDataWidth,

          RamWidthInstr     => RamWidthInstr
          )
      port map(
          clk                 => clk,
          rst                 => rst,
          addr_start_instr    => addr_start_instr,
          
          first_character     => first_character,
          ending_character    => ending_character,

          curr_character      => curr_character,
          curr_last_match_char=> curr_last_match_char,
          reload_bram         => reload_bram,
          reload_complete     => reload_complete,

          src_en              => src_en,
          complete            => complete,
          found               => found,
          where_complete      => where_complete,

          addr_instruction_1  => ram_instr_addr_rd_1_int,
          instruction_1       => ram_instr_out_1_int,
          addr_instruction_2  => ram_instr_addr_rd_2_int,
          instruction_2       => ram_instr_out_2_int,

          addr_instruction_3  => ram_instr_addr_rd_3_int,
          instruction_3       => ram_instr_out_3_int,

          addr_data           => ram_data_address_rd_int,
          data                => ram_data_out_int
          );
end Behavioral;
