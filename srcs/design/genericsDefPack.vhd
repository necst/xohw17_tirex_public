----------------------------------------------------------------------------------
-- Company: Politecnico di Milano
-- Student: Alessandro Comodi
-- Student: Davide Conficconi
-- 
-- Create Date: 03/21/2017 03:56:02 PM
-- Design Name: TiReX - single core
-- Module Name: GenericsPack - package
-- Project Name: TiReX
-- Target Devices: Virtex 7 series
-- Tool Versions: Vivado 16.4
-- Description: Package that contains all the parameters of the architecture.
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library ieee;
use ieee.std_logic_1164.all;

package GenericsPack is
    -- Width of the data in input (usually is of 8 bits)
    constant DataWidth          : positive := 8;

    -- Width of the Opcode in the instruction
    constant OpCodeWidth    	: positive := 6;
    -- Width of the decoded opcode. It is a one-hot vector in which
    -- each bit represents a different operator
    constant OpCodeBus		    : positive := 10;
    -- Width of the internal operators (OR and AND)
    constant InternalOpBus	   	: positive := 2;

    -- Width of each cluster (Number of Comparators)
    constant ClusterWidth 	   	: positive := 4;
    -- Number of clusters
    constant NCluster			: positive := 4;
    
    -- This determines the range of the Data Ram
    constant AddressWidthData	: positive := 5;

    constant AddrWidthDataBRAM  : positive := 10;
    -- This determines the range of the Instr Ram
    constant AddressWidthInstr	: positive := 4;
    -- This determines the width of each cell in the Data Ram
    constant RamWidthData		: positive := 8;
    -- This determines the width of the external bus of the AXI4 peripheral
    constant ExternalBusWidthData : positive := 128;

    constant ExternalBusWidthInstr : positive := 32;
    -- This determines how many bits can be written on the bus connecting the Data Ram to the CPU
    constant InternalBusWidthData       : positive := DataWidth * (NCluster + ClusterWidth - 1);
    -- This determines the width of each cell in the Instr Ram
    constant RamWidthInstr		: positive := ClusterWidth * DataWidth + OpCodeWidth;

    constant CounterWidth       : positive := 3;
    
    constant StackDataWidth     : positive := CounterWidth + OpCodeBus + 1 + AddressWidthInstr + AddressWidthData;
    constant BufferAddressWidth : positive := 3;

    constant CharacterNumber    : positive := AddrWidthDataBRAM + 4;
end GenericsPack;

package body GenericsPack is
end GenericsPack;
