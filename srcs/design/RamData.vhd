----------------------------------------------------------------------------------
-- Company: Politecnico di Milano
-- Student: Alessandro Comodi
-- Student: Davide Conficconi
-- 
-- Create Date: 03/21/2017 03:56:02 PM
-- Design Name: TiReX - single core
-- Module Name: RamData - behav
-- Project Name: TiReX
-- Target Devices: Virtex 7 series
-- Tool Versions: Vivado 16.4
-- Description: This component is the Ram dedicated to the storage of the data.
--				Data refers to the various characters which have to be checked
--				by the comparators of the Execute module in the Core.
-- 				This Ram is not included in the TiReX core, but it is external 
--				to it.				
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity RamData is
	generic (
		-- Length of a single word stored in a ram cell
		RamWidth			: positive := 8;
		-- Number of bits that can be written to the output port of the Ram
		InternalBusWidth 	: positive := 32;
		-- Number of bits  that can be written in the Ram
		ExternalBusWidth	: positive := 128;
		-- Number of bits that defines the length of the address. The DataRam
		-- will have 2^AddressWidth cells
		AddressWidth		: positive := 8
		);
	port (
		-- reset signal. Active low
		rst				: in std_logic;
		-- clock signal
		clk				: in std_logic;
		-- read address
		address_rd 		: in std_logic_vector(AddressWidth - 1 downto 0);
		-- write address
		address_wr		: in std_logic_vector(AddressWidth - 1 downto 0);
		-- data to be stored at the write address
		data_in			: in std_logic_vector(ExternalBusWidth - 1 downto 0);
		-- signal of write enable
		we 				: in std_logic;
		-- data read, relative to the read address
		data_out 		: out std_logic_vector(InternalBusWidth - 1 downto 0)
		);
end RamData;

architecture behav of RamData is
	-- The Data Ram is composed by an array that exceeds the specified AddressWidth because it
	-- otherwise the last cells of the array could not be read because data_out_1 would be undefined
	type RAM is array(0 to 2**AddressWidth - 1) of std_logic_vector(RamWidth - 1 downto 0);
	signal b_ram : RAM;
begin
	read : process (clk, rst, address_rd) 
	begin
		if rst = '0' then
			data_out <= (others => '0');
		else
			-- This loops collects all the data from the cell addressed by address_rd_1 to BusWidth / RamWidth cells ahead.
			-- This becuase the bus can contain more data than the RamWidth.
			for i in InternalBusWidth / RamWidth downto 1 loop
					data_out(i * RamWidth - 1 downto RamWidth * (i - 1)) <= b_ram((to_integer(unsigned(address_rd)) + InternalBusWidth / RamWidth - i) mod 2**AddressWidth);
			end loop;
			-- Data is read everytime address_rd_1 changes
		end if;
	end process read;

	write : process (clk, rst)
	begin
		-- If rst is low the array is set to zero
		if rst = '0' then
			b_ram <= (others => (others => '0'));
		else
			-- for each rising edge of the clock, if write enable is low, data is written at the specified address
			if rising_edge(clk) then
				if we = '1' then
					for i in ExternalBusWidth / RamWidth downto 1 loop
						b_ram(to_integer(unsigned(address_wr)) + ExternalBusWidth / RamWidth - i)	<= data_in(i * RamWidth - 1 downto (i - 1) * RamWidth);
					end loop;
				end if;
			end if;
		end if;
	end process write;
end behav;
