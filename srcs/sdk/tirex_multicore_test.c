/******************************************************************************
*
* Copyright (C) 2009 - 2014 Xilinx, Inc.  All rights reserved.
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
*
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
*
* Use of the Software is limited solely to applications:
* (a) running on a Xilinx device, or
* (b) that interact with a Xilinx device through a bus or interconnect.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
* XILINX  BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
* WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF
* OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
* SOFTWARE.
*
* Except as contained in this notice, the name of the Xilinx shall not be used
* in advertising or otherwise to promote the sale, use or other dealings in
* this Software without prior written authorization from Xilinx.
*
******************************************************************************/

/*
 *
 *
 * This application configures UART 16550 to baud rate 9600.
 * PS7 UART (Zynq) is not initialized by this application, since
 * bootrom/bsp configures it to baud rate 115200
 *
 * ------------------------------------------------
 * | UART TYPE   BAUD RATE                        |
 * ------------------------------------------------
 *   uartns550   9600
 *   uartlite    Configurable only in HW design
 *   ps7_uart    115200 (configured by bootrom/bsp)
 *
 *
 * This application is used to test the TiReX core. It controls the
 * microblaze which then sends all the control signals and data to the
 * core through AXI Lite.
 */

#include <stdio.h>
#include "platform.h"
#include "xparameters.h"
#include "xil_io.h"
#include "tirex_core_ip.h"
#include "xil_printf.h"
#include "xil_types.h"
#include "tirex_core_test.h"

#define DEBUG_INSTR_MEM 0

int main()
{
    init_platform();
    int i;
    int j;
    /*
        The following constants define:
            - The starting address of the instructions
            - The starting character of the data in the BRAM
            - The ending character in the BRAM
    */
    const u32 instr_start_addr  = 0;
    const u32 data_start_addr   = 0;
    const u32 data_end_addr     = 16000;

    u32 tirex_cores_base_addr[] = {
        XPAR_TIREX_CORE_IP_0_S00_AXI_BASEADDR,
        XPAR_TIREX_CORE_IP_1_S00_AXI_BASEADDR,
        XPAR_TIREX_CORE_IP_2_S00_AXI_BASEADDR,
        XPAR_TIREX_CORE_IP_3_S00_AXI_BASEADDR
    };

    // Instruction address needed to load instruction in the Instruction
    // Memory of the core
    u32 instr_address   = 0;

    /*
        Control Vector that holds all the control bits needed by the core

        VALUE       MEANING
        0x0         Core disabled
        0x1         Search of the core but core disabled
        0x2         Write the Instruction reference data
        0x4         Write the Instruction opcode
        0x8         Enable the core
        0x9         Enable the core and the search
        0x10        Enable BRAM debug mode
        0x20        Reset the system
    */
    u32 control 		= 0;

    // Data in AXI register 7 that holds the results of the computation
    u32 result[]          = {0, 0};

    // Data in AXI register 8 that holds the character index of the matching string
    u32 char_num[]        = {0, 0};

    // These variable are filled when the computation terminates and are needed
    // to display the results
    u8 found			= 0;
    u32 timing_counter[]  = {0, 0};

    /*
        This array holds the instructions to be loaded into the
        instruction memory. The instruction are organized as follows:
            - first dimension   : reference data
            - second dimension  : opcode

        OPCODE      MEANING
        0x7         JMP
        0x8         OR
        0x9         OR )*
        0xa         OR )+
        0xb         OR )|
        0xc         OR )
        0x10        AND
        0x11        AND )*
        0x12        AND )+
        0x13        AND )|
        0x14        AND )
        0x20        (

        The array must be filled with the instruction provided by the compiler in
        order to test the regular expression
    */

    // Resetting the system
    control = 0x20;

    for(i = 0; i < sizeof(tirex_cores_base_addr) / sizeof(u32); i++){
        TIREX_CORE_IP_mWriteReg(tirex_cores_base_addr[i],
                               TIREX_CORE_IP_S00_AXI_SLV_REG7_OFFSET,
                               control);
        xil_printf("Resetting the core %d\n\n", i);
    }

    xil_printf("Initializing Instruction RAM of Tirex core...\n\n");
    control = 0x0;
    for(i = 0; i < sizeof(tirex_cores_base_addr) / sizeof(u32); i++){
        TIREX_CORE_IP_mWriteReg(tirex_cores_base_addr[i],
                               TIREX_CORE_IP_S00_AXI_SLV_REG7_OFFSET,
                               control);
        xil_printf("Disabling the core %d\n\n", i);
    }

    xil_printf("Debug\n\n");
    /* Check if the core output is clean
    result = TIREX_CORE_IP_mReadReg(XPAR_TIREX_CORE_IP_0_S00_AXI_BASEADDR,
                                      TIREX_CORE_IP_S00_AXI_SLV_REG7_OFFSET);

    xil_printf("\tcomplete = %d\n", result & (1 << 0));
    xil_printf("\tfound = %d\n", result & (1 << 1));

    /**
     *	Fill the instruction RAM of the core
     */
    for(i = 0; i < sizeof(instr) / sizeof(u32) / 2; i++){
		for(j = 0; j < sizeof(tirex_cores_base_addr) / sizeof(u32); j++){
            TIREX_CORE_IP_mWriteReg(tirex_cores_base_addr[j],
    								TIREX_CORE_IP_S00_AXI_SLV_REG0_OFFSET,
    								instr_address);
        }

    	control = 0x2;
        for(j = 0; j < sizeof(tirex_cores_base_addr) / sizeof(u32); j++){
            TIREX_CORE_IP_mWriteReg(tirex_cores_base_addr[j],
                                    TIREX_CORE_IP_S00_AXI_SLV_REG7_OFFSET,
                                    control);

    		TIREX_CORE_IP_mWriteReg(tirex_cores_base_addr[j],
    								TIREX_CORE_IP_S00_AXI_SLV_REG1_OFFSET,
    								instr[i][0]);
        }

		control = 0x4;
        for(j = 0; j < sizeof(tirex_cores_base_addr) / sizeof(u32); j++){
            TIREX_CORE_IP_mWriteReg(tirex_cores_base_addr[j],
                                    TIREX_CORE_IP_S00_AXI_SLV_REG7_OFFSET,
                                    control);

    		TIREX_CORE_IP_mWriteReg(tirex_cores_base_addr[j],
    								TIREX_CORE_IP_S00_AXI_SLV_REG1_OFFSET,
    								instr[i][1]);
        }

        if(DEBUG_INSTR_MEM)
    		xil_printf("Instruction:\n"
    				"\topcode --> %x\n"
    				"\treference --> %x\n", instr[i][1], instr[i][0]);


        instr_address += 1;
    }

    /*
     * Set up of the core for the search
     */
    xil_printf("Setting up the core for the search...\n\r");
    control = 0x8;
    for(i = 0; i < sizeof(tirex_cores_base_addr) / sizeof(u32); i++){
        TIREX_CORE_IP_mWriteReg(tirex_cores_base_addr[i],
                                       TIREX_CORE_IP_S00_AXI_SLV_REG7_OFFSET,
                                       control);
        TIREX_CORE_IP_mWriteReg(tirex_cores_base_addr[i],
                                TIREX_CORE_IP_S00_AXI_SLV_REG4_OFFSET,
                                data_start_addr);
        TIREX_CORE_IP_mWriteReg(tirex_cores_base_addr[i],
                                TIREX_CORE_IP_S00_AXI_SLV_REG5_OFFSET,
                                data_end_addr);

        TIREX_CORE_IP_mWriteReg(tirex_cores_base_addr[i],
                                TIREX_CORE_IP_S00_AXI_SLV_REG6_OFFSET,
                                instr_start_addr);
    }

    xil_printf("Core ready for the search... \n");

    /*
     * Enable the search
     */
    control = 0x9;
    for(i = 0; i < sizeof(tirex_cores_base_addr) / sizeof(u32); i++){
        TIREX_CORE_IP_mWriteReg(tirex_cores_base_addr[i],
                                TIREX_CORE_IP_S00_AXI_SLV_REG7_OFFSET,
                                control);
    }


    /*
        Polling in order to check the completion of the core computation
    */
    while(!(result[0] & (1 << 0)) && !(result[1] & (1 << 0))){
    	xil_printf("Searching...\n");
        for(i = 0; i < sizeof(tirex_cores_base_addr) / sizeof(u32); i++){
            result[i] = TIREX_CORE_IP_mReadReg(tirex_cores_base_addr[i],
            		TIREX_CORE_IP_S00_AXI_SLV_REG7_OFFSET);
        }
    }

    // Result retrevial
    for(i = 0; i < sizeof(tirex_cores_base_addr) / sizeof(u32); i++){
        result[i] = TIREX_CORE_IP_mReadReg(tirex_cores_base_addr[i],
                TIREX_CORE_IP_S00_AXI_SLV_REG7_OFFSET);
    }

    // Counter measurement retrevial
    for(i = 0; i < sizeof(tirex_cores_base_addr) / sizeof(u32); i++){
        timing_counter[i]  = TIREX_CORE_IP_mReadReg(tirex_cores_base_addr[i],
        		          TIREX_CORE_IP_S00_AXI_SLV_REG6_OFFSET);
    }
    // Disabling the cores
    control = 0x0;
    for(i = 0; i < sizeof(tirex_cores_base_addr) / sizeof(u32); i++){
        TIREX_CORE_IP_mWriteReg(tirex_cores_base_addr[i],
                                    TIREX_CORE_IP_S00_AXI_SLV_REG7_OFFSET,
                                    control);
    }

    for(i = 0; i < sizeof(tirex_cores_base_addr) / sizeof(u32); i++){
        xil_printf("Core %d has completed the search in: %d clock cycles\n\n", i, timing_counter[i]);
        xil_printf("Core %d  has completed the search ----->\n\t complete = %d\n\n\t found = %d\n\n", i, result[i] & (1 << 0), result[i] & (1 << 1));
    }

    if((result[0] & (1 << 1)) || (result[1] & (1 << 1)))
    	found = 1;
    else
    	found = 0;

    // Last match retrievial
    if(found)
    {
        for(i = 0; i < sizeof(tirex_cores_base_addr) / sizeof(u32); i++){
            char_num[i] = TIREX_CORE_IP_mReadReg(tirex_cores_base_addr[i],
            		TIREX_CORE_IP_S00_AXI_SLV_REG8_OFFSET);

            xil_printf("Last match character index of core %d ------> %d\n\n", i, char_num[i]);
        }
    }

    // Resetting the core
    control = 0x20;
    for(i = 0; i < sizeof(tirex_cores_base_addr) / sizeof(u32); i++){
        TIREX_CORE_IP_mWriteReg(tirex_cores_base_addr[i],
                                       TIREX_CORE_IP_S00_AXI_SLV_REG7_OFFSET,
                                       control);
    }

    cleanup_platform();
    return 0;
}

