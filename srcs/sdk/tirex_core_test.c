/******************************************************************************
*
* Copyright (C) 2009 - 2014 Xilinx, Inc.  All rights reserved.
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
*
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
*
* Use of the Software is limited solely to applications:
* (a) running on a Xilinx device, or
* (b) that interact with a Xilinx device through a bus or interconnect.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
* XILINX  BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
* WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF
* OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
* SOFTWARE.
*
* Except as contained in this notice, the name of the Xilinx shall not be used
* in advertising or otherwise to promote the sale, use or other dealings in
* this Software without prior written authorization from Xilinx.
*
******************************************************************************/

/*
 *
 *
 * This application configures UART 16550 to baud rate 9600.
 * PS7 UART (Zynq) is not initialized by this application, since
 * bootrom/bsp configures it to baud rate 115200
 *
 * ------------------------------------------------
 * | UART TYPE   BAUD RATE                        |
 * ------------------------------------------------
 *   uartns550   9600
 *   uartlite    Configurable only in HW design
 *   ps7_uart    115200 (configured by bootrom/bsp)
 *
 *
 * This application is used to test the TiReX core. It controls the
 * microblaze which then sends all the control signals and data to the
 * core through AXI Lite.
 */

#include <stdio.h>
#include "platform.h"
#include "xparameters.h"
#include "xil_io.h"
#include "tirex_core_ip.h"
#include "xil_printf.h"
#include "xil_types.h"
#include "tirex_core_test.h"

#define DEBUG_BRAM_DATA 0
#define DEBUG_INSTR_MEM 0
#define MANUAL_BRAM_LOAD 0

int main()
{
    init_platform();
    int i;

    /*
        The following constants define:
            - The starting address of the instructions
            - The starting character of the data in the BRAM
            - The ending character in the BRAM
    */
    const u32 instr_start_addr  = 0;
    const u32 data_start_addr   = 0;
    const u32 data_end_addr     = 16000;

    // Address needed by the MicroBlaze to write/read at a certain
    // address in the BRAM
    u32 data_address	= data_start_addr;
    // Control signal to tell to the BRAM if or not to write
    u32 data_we         = 0x0;

    // Instruction address needed to load instruction in the Instruction
    // Memory of the core
    u32 instr_address   = 0x0;

    /*
        Control Vector that holds all the control bits needed by the core

        VALUE       MEANING
        0x0         Core disabled
        0x1         Search of the core but core disabled
        0x2         Write the Instruction reference data
        0x4         Write the Instruction opcode
        0x8         Enable the core
        0x9         Enable the core and the search
        0x10        Enable BRAM debug mode
        0x20        Reset the system
    */
    u32 control 		= 0x0;

    // Data in AXI register 7 that holds the results of the computation
    u32 result          = 0x0;

    // Data in AXI register 8 that holds the character index of the matching string
    u32 char_num        = 0x0;

    // These variable are filled when the computation terminates and are needed
    // to display the results
    u8 found			= 0;
    u8 end_data			= 0;
    u8 correct_match	= 0;
    u32 timing_counter  = 0;

    // This array serves to display the data coming from the BRAM
    u32 bram_data[4];

    // This array is filled with testing data if the user wants to
    // manually set the BRAM
    u32 data[] = {};

    /*
        This array holds the instructions to be loaded into the
        instruction memory. The instruction are organized as follows:
            - first dimension   : reference data
            - second dimension  : opcode

        OPCODE      MEANING
        0x7         JMP
        0x8         OR
        0x9         OR )*
        0xa         OR )+
        0xb         OR )|
        0xc         OR )
        0x10        AND
        0x11        AND )*
        0x12        AND )+
        0x13        AND )|
        0x14        AND )
        0x20        (

        The array must be filled with the instruction provided by the compiler in
        order to test the regular expression
    */

    // Resetting the system
    control = 0x20;

    TIREX_CORE_IP_mWriteReg(XPAR_TIREX_CORE_IP_0_S00_AXI_BASEADDR,
                                   TIREX_CORE_IP_S00_AXI_SLV_REG7_OFFSET,
                                   control);

    xil_printf("Initializing Instruction RAM of Tirex core...\n");
    control = 0x0;
    TIREX_CORE_IP_mWriteReg(XPAR_TIREX_CORE_IP_0_S00_AXI_BASEADDR,
                                   TIREX_CORE_IP_S00_AXI_SLV_REG7_OFFSET,
                                   control);

    // Check if the core output is clean
    result = TIREX_CORE_IP_mReadReg(XPAR_TIREX_CORE_IP_0_S00_AXI_BASEADDR,
                                      TIREX_CORE_IP_S00_AXI_SLV_REG7_OFFSET);

    xil_printf("\tcomplete = %d\n", result & (1 << 0));
    xil_printf("\tfound = %d\n", result & (1 << 1));

    /**
     *	Fill the instruction RAM of the core
     */
    for(i = 0; i < sizeof(instr) / sizeof(u32) / 2; i++){
		TIREX_CORE_IP_mWriteReg(XPAR_TIREX_CORE_IP_0_S00_AXI_BASEADDR,
								TIREX_CORE_IP_S00_AXI_SLV_REG0_OFFSET,
								instr_address);
    	control = 0x2;
        TIREX_CORE_IP_mWriteReg(XPAR_TIREX_CORE_IP_0_S00_AXI_BASEADDR,
                                TIREX_CORE_IP_S00_AXI_SLV_REG7_OFFSET,
                                control);

		TIREX_CORE_IP_mWriteReg(XPAR_TIREX_CORE_IP_0_S00_AXI_BASEADDR,
								TIREX_CORE_IP_S00_AXI_SLV_REG1_OFFSET,
								instr[i][0]);

    	control = 0x4;
        TIREX_CORE_IP_mWriteReg(XPAR_TIREX_CORE_IP_0_S00_AXI_BASEADDR,
                                TIREX_CORE_IP_S00_AXI_SLV_REG7_OFFSET,
                                control);

		TIREX_CORE_IP_mWriteReg(XPAR_TIREX_CORE_IP_0_S00_AXI_BASEADDR,
								TIREX_CORE_IP_S00_AXI_SLV_REG1_OFFSET,
								instr[i][1]);

        if(DEBUG_INSTR_MEM)
    		xil_printf("Instruction:\n"
    				"\topcode --> %x\n"
    				"\treference --> %x\n", instr[i][1], instr[i][0]);


        instr_address += 1;
    }

    /*
     * Fill the data RAM of the core if needed
     */
    if(MANUAL_BRAM_LOAD)
    {
        xil_printf("Initializing Data RAM of Tirex core...\n");
        data_we = 0x1;
        TIREX_CORE_IP_mWriteReg(XPAR_TIREX_CORE_IP_0_S00_AXI_BASEADDR,
                                TIREX_CORE_IP_S00_AXI_SLV_REG2_OFFSET,
                                data_we);

        for(i = 0; i < sizeof(data) / sizeof(u32); i++){
            TIREX_CORE_IP_mWriteReg(XPAR_TIREX_CORE_IP_0_S00_AXI_BASEADDR,
                                TIREX_CORE_IP_S00_AXI_SLV_REG8_OFFSET,
                                data_address);

            TIREX_CORE_IP_mWriteReg(XPAR_TIREX_CORE_IP_0_S00_AXI_BASEADDR,
                                TIREX_CORE_IP_S00_AXI_SLV_REG9_OFFSET,
                                data[i]);

            data_address += 1;
        }

        data_we = 0x0;

        TIREX_CORE_IP_mWriteReg(XPAR_TIREX_CORE_IP_0_S00_AXI_BASEADDR,
                TIREX_CORE_IP_S00_AXI_SLV_REG2_OFFSET,
                data_we);
    }

    //
    if(DEBUG_BRAM_DATA)
    {
        xil_printf("Checking data in BRAM\n");

        control = 0x10;
        data_address = 0x0;

        TIREX_CORE_IP_mWriteReg(XPAR_TIREX_CORE_IP_0_S00_AXI_BASEADDR,
                TIREX_CORE_IP_S00_AXI_SLV_REG7_OFFSET,
                control);

        for(i = 0; i < sizeof(data) / sizeof(u32) / 4; i++){
        	TIREX_CORE_IP_mWriteReg(XPAR_TIREX_CORE_IP_0_S00_AXI_BASEADDR,
        	            TIREX_CORE_IP_S00_AXI_SLV_REG3_OFFSET,
        	            data_address);

        	bram_data[0] = TIREX_CORE_IP_mReadReg(XPAR_TIREX_CORE_IP_0_S00_AXI_BASEADDR,
                    TIREX_CORE_IP_S00_AXI_SLV_REG2_OFFSET);
        	bram_data[1] = TIREX_CORE_IP_mReadReg(XPAR_TIREX_CORE_IP_0_S00_AXI_BASEADDR,
                    TIREX_CORE_IP_S00_AXI_SLV_REG3_OFFSET);
        	bram_data[2] = TIREX_CORE_IP_mReadReg(XPAR_TIREX_CORE_IP_0_S00_AXI_BASEADDR,
                    TIREX_CORE_IP_S00_AXI_SLV_REG4_OFFSET);
        	bram_data[3] = TIREX_CORE_IP_mReadReg(XPAR_TIREX_CORE_IP_0_S00_AXI_BASEADDR,
                    TIREX_CORE_IP_S00_AXI_SLV_REG5_OFFSET);

        	xil_printf("Data at address %x\n"
        			"\t %x%x%x%x\n", data_address, bram_data[3], bram_data[2], bram_data[1], bram_data[0]);

        	data_address += 1;
        }

        data_address = 0x0;
    	TIREX_CORE_IP_mWriteReg(XPAR_TIREX_CORE_IP_0_S00_AXI_BASEADDR,
    	            TIREX_CORE_IP_S00_AXI_SLV_REG3_OFFSET,
    	            data_address);
    }

    /*
     * Set up of the core for the search
     */
    xil_printf("Setting up the core for the search...\n\r");
    control = 0x8;

    TIREX_CORE_IP_mWriteReg(XPAR_TIREX_CORE_IP_0_S00_AXI_BASEADDR,
                                   TIREX_CORE_IP_S00_AXI_SLV_REG7_OFFSET,
                                   control);
    TIREX_CORE_IP_mWriteReg(XPAR_TIREX_CORE_IP_0_S00_AXI_BASEADDR,
                            TIREX_CORE_IP_S00_AXI_SLV_REG4_OFFSET,
                            data_start_addr);
    TIREX_CORE_IP_mWriteReg(XPAR_TIREX_CORE_IP_0_S00_AXI_BASEADDR,
                            TIREX_CORE_IP_S00_AXI_SLV_REG5_OFFSET,
                            data_end_addr);

    TIREX_CORE_IP_mWriteReg(XPAR_TIREX_CORE_IP_0_S00_AXI_BASEADDR,
                            TIREX_CORE_IP_S00_AXI_SLV_REG6_OFFSET,
                            instr_start_addr);
    xil_printf("Core ready for the search... \n");

    /*
     * Enable the search
     */
    control = 0x9;
    TIREX_CORE_IP_mWriteReg(XPAR_TIREX_CORE_IP_0_S00_AXI_BASEADDR,
                            TIREX_CORE_IP_S00_AXI_SLV_REG7_OFFSET,
                            control);

    /*
        Polling in order to check the completion of the core computation
    */
    while(!(result & (1 << 0))){
    	xil_printf("Searching...\n");
        result = TIREX_CORE_IP_mReadReg(XPAR_TIREX_CORE_IP_0_S00_AXI_BASEADDR,
        		TIREX_CORE_IP_S00_AXI_SLV_REG7_OFFSET);
    }

    // Result retrevial
    result 	        = TIREX_CORE_IP_mReadReg(XPAR_TIREX_CORE_IP_0_S00_AXI_BASEADDR,
            		  TIREX_CORE_IP_S00_AXI_SLV_REG7_OFFSET);

    // Counter measurement retrevial
    timing_counter  = TIREX_CORE_IP_mReadReg(XPAR_TIREX_CORE_IP_0_S00_AXI_BASEADDR,
    		          TIREX_CORE_IP_S00_AXI_SLV_REG6_OFFSET);

    // Disabling the core
    control = 0x0;
    TIREX_CORE_IP_mWriteReg(XPAR_TIREX_CORE_IP_0_S00_AXI_BASEADDR,
                                TIREX_CORE_IP_S00_AXI_SLV_REG7_OFFSET,
                                control);

    xil_printf("The core has completed the search in: %d clock cycles\n\n\n", timing_counter);
    xil_printf("The core has completed the search -----> complete = %d\n\n", result & (1 << 0));
    if(result & (1 << 1))
    	found = 1;
    else
    	found = 0;
    xil_printf("The core has completed the search -----> found = %d\n\n", found);

    if(result & (1 << 2))
    	end_data = 1;
    xil_printf("\tEnd of data\t\t = %d\n\n", end_data);

    if(result & (1 << 4))
    	correct_match = 1;
    xil_printf("\tCorrectly complete\t = %d\n\n", correct_match);

    // Last match retrievial
    if(found)
    {
        char_num = TIREX_CORE_IP_mReadReg(XPAR_TIREX_CORE_IP_0_S00_AXI_BASEADDR,
        		TIREX_CORE_IP_S00_AXI_SLV_REG8_OFFSET);

        xil_printf("Last match character index ------> %d", char_num);
    }

    // Resetting the core
    control = 0x20;
    TIREX_CORE_IP_mWriteReg(XPAR_TIREX_CORE_IP_0_S00_AXI_BASEADDR,
                                   TIREX_CORE_IP_S00_AXI_SLV_REG7_OFFSET,
                                   control);

    cleanup_platform();
    return 0;
}
