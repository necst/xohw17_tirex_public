Opcode outgoing from F&D stage has 10 bits [0:9]

0 1 2 3 4 5 6 7 8 9			Meaning 

0 0 0 0 0 0 0 0 0 1			Jump instruction "jmp"

0 0 0 0 0 0 1 0 0 0 		Open parenthesis "("

0 0 1 0 0 0 0 0 0 0 		"NOP" instruction

0 0 0 0 0 0 0 0 1 0 		Always instr. "."

1 0 0 0 0 0 0 0 0 0 		Simple "OR" instr.

0 1 0 0 0 0 0 0 0 0 		Simple "AND" instr.

1 0 0 1 0 0 0 0 0 0 		Reference in OR, than
							kleene plus "| )+"

0 1 0 1 0 0 0 0 0 0 		Reference in AND, than
							kleene plus "& )+"

1 0 0 0 1 0 0 0 0 0 		Reference in OR, then
							kleene star "| )*"

0 1 0 0 1 0 0 0 0 0 		Reference in AND, then
							kleene star "& )*"

1 0 0 0 0 1 0 0 0 0 		Reference in OR, then
							closed parenthesis, OR between
							parenthesis "| )|"

0 1 0 0 0 1 0 0 0 0 		Reference in AND, then
							closed parenthesis, OR between
							parenthesis "& )|"

1 0 0 0 0 0 0 1 0 0 		Reference in OR then closed
							parenthesis "| )"

0 1 0 0 0 0 0 1 0 0 		Reference in AND then closed
							parenthesis "& )"


Format of instructions incoming in F&D stage

 +---+-----+-------+
 ¦ 5 ¦ 4 3 ¦ 2 1 0 ¦
 +---+-----+-------+
 ¦ ( ¦    |¦    )| ¦
 ¦   ¦ &   ¦     )*¦
 ¦   ¦     ¦   )+  ¦
 ¦   ¦     ¦ )     ¦
 |   | . . |       |
 |	 |	   | j m p |
 +---+-----+-------+