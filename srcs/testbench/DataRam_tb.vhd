----------------------------------------------------------------------------------
-- Company: Politecnico di Milano
-- Author: Alessandro Comodi 
-- 
-- Create Date: 03/29/2017 11:45:47 AM
-- Design Name: TiReX - single core
-- Module Name: DataRam_tb - behav
-- Project Name: TiReX
-- Target Devices: Virtex 7 series
-- Tool Versions: Vivado 16.4
-- Description: This module is a testbench for the Ram used for storing data. 
--				It instantiates a Ram entity and after resetting it it fills
--				the array with incremental data and then reads it testing if
--				it has been correctly populated.				
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.std_logic_unsigned.all;
use work.GenericsPack.all;

entity DataRam_tb is
end DataRam_tb;

architecture behav of DataRam_tb is

	-- constants for the RamData entity

	constant RamWidthData			: positive := 8;
	constant AddressWidthData		: positive := 6;
	constant ClusterWidth			: positive := 4;
	constant NCluster				: positive := 4;
	constant DataWidth				: positive := 8;
	constant ExternalBusWidthData	: positive := 128;
	constant InternalBusWidthData	: positive := DataWidth * (ClusterWidth + NCluster - 1);

	-- clock signal
	signal clk		: std_logic := '0';
	-- address from which the data is read or to which data_in is written
	signal address_rd 	: std_logic_vector(AddressWidthData - 1 downto 0) := (others => '0');
	-- address to which data_in is written
	signal address_wr 	: std_logic_vector(AddressWidthData - 1 downto 0) := (others => '0');
	-- data to be written at the specified address if "we" is low	
	signal data_in	: std_logic_vector(ExternalBusWidthData - 1 downto 0);
	-- signal to control the writing. active low
	signal we 		: std_logic := '1';
	-- signal referring to the output of the ram
	signal data_out	: std_logic_vector(InternalBusWidthData - 1 downto 0);
	-- reset signal
	signal rst		: std_logic := '1';

begin
	DATA_RAM	: entity work.RamData
		generic map(
			RamWidth			=> RamWidthData,
			InternalBusWidth	=> InternalBusWidthData,
			ExternalBusWidth	=> ExternalBusWidthData,
			AddressWidth		=> AddressWidthData		
			)
		port map(
			clk 			=> clk,
			rst 			=> rst,
			address_rd 		=> address_rd,
			address_wr		=> address_wr,
			data_in			=> data_in,
			we 				=> we,
			data_out		=> data_out
			);

	clk <= not clk after 5 ns;

	process 
	begin
		rst <= '0';
		we 	<= '0';

		wait until rising_edge(clk);
		
		we <= '1';
		rst <= '1';

		wait until rising_edge(clk);
		
		for j in 0 to (2**AddressWidthData) / (ExternalBusWidthData / RamWidthData) - 1 loop
	        
			-- This for loop populates the data_in signal. Each element that is loaded in the Data Buffer
			-- are calculated as "i * j"
	        for i in 1 to ExternalBusWidthData / RamWidthData loop
	            data_in(ExternalBusWidthData - (i - 1) * RamWidthData - 1 downto ExternalBusWidthData - i * RamWidthData) <= 
	                                    std_logic_vector(to_unsigned(j * i, RamWidthData)); 
	        end loop;

            wait until rising_edge(clk);
            address_wr	<= address_wr + ExternalBusWidthData / RamWidthData;
        end loop;

        we <= '0';

        wait until rising_edge(clk);

        -- This for loop checks if the data has been correctly set in the Data Buffer
		for i in 0 to (2**AddressWidthData) / (InternalBusWidthData / RamWidthData) - 1 loop
			wait until rising_edge(clk);
			
			address_rd <= address_rd + InternalBusWidthData / RamWidthData;
		end loop;

		wait until rising_edge(clk);
		
		address_rd <= "000000";

		wait until falling_edge(clk);

		-- This test checks if at address '0' all the data coming from the ram are zero 
		-- as they were loaded in the first part of the test.
		for i in InternalBusWidthData / RamWidthData downto 1 loop
			assert data_out(DataWidth * i - 1 downto DataWidth * (i - 1)) = "00000000"
				report "First test failed. Ram Data does not contain the right values!" severity failure;
		end loop;

		wait until rising_edge(clk);

		address_rd <= address_rd + ExternalBusWidthData / RamWidthData;

		wait until falling_edge(clk);

		-- This test checks if the data at address 16 are correct. They must go from 1 to 7
		for i in InternalBusWidthData / RamWidthData downto 1 loop
			assert data_out(DataWidth * i - 1 downto DataWidth * (i - 1)) = InternalBusWidthData / RamWidthData - (i - 1)
				report "Second test failed. Ram Data does not contain the right values!" severity failure;
		end loop;

		wait until rising_edge(clk);

		address_rd <= address_rd + ExternalBusWidthData / RamWidthData;

		wait until falling_edge(clk);

		-- This test checks seven numbers loaded in the data buffer. They should be 2, 4, 6, 8, a, c, e
		for i in InternalBusWidthData / RamWidthData downto 1 loop
			assert data_out(DataWidth * i - 1 downto DataWidth * (i - 1)) = (InternalBusWidthData / RamWidthData - (i - 1)) * 2
				report "Third test failed. Ram Data does not contain the right values!" severity failure;
		end loop;

		wait;
	end process;
end behav;
