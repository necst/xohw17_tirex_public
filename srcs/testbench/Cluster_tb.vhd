----------------------------------------------------------------------------------
-- Company: Politecnico di Milano
-- Engineer: Alessandro Comodi
-- Engineer: Davide Conficconi
-- 
-- Create Date: 03/21/2017 03:56:02 PM
-- Design Name: TiReX - single core
-- Module Name: Cluster_tb - behav
-- Project Name: TiReX
-- Target Devices: Virtex 7 series
-- Tool Versions: Vivado 16.4
-- Description: This module serves as test in order to verify the correct
--				behaviour of the comparator clusters. the result signal has
--				to have a '1' corresponding to the comparator that performed
-- 				the match.
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.std_logic_unsigned.all;
use work.TypesPack.all;

entity Cluster_tb is
end Cluster_tb;

architecture behav of Cluster_tb is
	
	signal clk				: std_logic := '0';

	constant ClusterWidth	: positive := 4;
	constant DataWidth		: positive := 8;	

	-------------------------------------
	--------- Internal  signals ---------
	-------------------------------------
	signal en 			: std_logic;
	signal op_or		: std_logic;
	signal data 		: ClusterArray(0 to ClusterWidth - 1);
	signal reference	: ClusterArray(0 to ClusterWidth - 1);
	signal result 		: std_logic_vector(ClusterWidth - 1 downto 0);
begin
	
	CLUSTER : entity work.ClusterMux
		generic map(
			ClusterWidth	=> ClusterWidth,
			DataWidth		=> DataWidth
			)
		port map(
			en 				=> en,
			opcode_is_or	=> op_or,
			data 			=> data,
			reference		=> reference,
			result			=> result
			);

	clk <= not clk after 5 ns;

	process
	begin
		en 				<= '1';
		op_or			<= '0';

		wait until rising_edge(clk);

		en 				<= '0';

		data(0) 		<= "01000001";
		data(1) 		<= "01000010";
		data(2)			<= "01000100";
		data(3)			<= "01000010";

		reference(0) 	<= "01000001";
		reference(1) 	<= "01000001";
		reference(2) 	<= "01000001";
		reference(3) 	<= "01000001";

		wait until falling_edge(clk);

		assert result = "1000"
			report "First test failed!" severity failure;

		wait until rising_edge(clk);

		data(0) 		<= "01000010";
		data(1) 		<= "01000011";
		data(2)			<= "01000100";
		data(3)			<= "01000010";

		reference(0) 	<= "01000010";
		reference(1) 	<= "01000011";
		reference(2) 	<= "01000100";
		reference(3) 	<= "01000010";

		wait until falling_edge(clk);

		assert result = "1111"
			report "Second test failed!" severity failure;

		wait until rising_edge(clk);

		op_or			<= '1';
		data(0) 		<= "01000000";
		data(1) 		<= "01000001";
		data(2)			<= "01000100";
		data(3)			<= "01000010";

		reference(0) 	<= "01000001";
		reference(1) 	<= "01000001";
		reference(2)	<= "01000010";
		reference(3)	<= "01000010";

		wait until falling_edge(clk);

		assert result = "0000"
			report "Third test failed!" severity failure;

		wait until rising_edge(clk);

		en 				<= '1';
		op_or			<= '0';

		data(0) 		<= "01000000";
		data(1) 		<= "01000001";
		data(2)			<= "01001100";
		data(3)			<= "01000010";

		reference(0) 	<= "01000001";
		reference(1) 	<= "01000001";
		reference(2)	<= "01001100";
		reference(3)	<= "01000010";

		wait until falling_edge(clk);

		assert result = "0000"
			report "Fourth test failed!" severity failure;

		wait until rising_edge(clk);

		en 				<= '0';
		op_or 			<= '0';

		data(0) 		<= "01010101";
		data(1)			<= "10101010";
		data(2)			<= "01000100";
		data(3)			<= "01000010";

		reference(0)	<= "01010101";
		reference(1) 	<= "10101010";
		reference(2)	<= "01000100";
		reference(3)	<= "01000010";

		wait until falling_edge(clk);

		assert result = "1111"
			report "Fifth test failed!" severity failure;

		wait until rising_edge(clk);

		op_or 			<= '1';

		data(0) 		<= "01010101";
		data(1)			<= "10101010";
		data(2)			<= "01000100";
		data(3)			<= "01000010";

		reference(0)	<= "00000101";
		reference(1) 	<= "10101010";
		reference(2)	<= "01000100";
		reference(3)	<= "01010101";

		wait until falling_edge(clk);

		assert result = "0001"
			report "Sixth test failed!" severity failure;

		wait until rising_edge(clk);

		assert false report "Simulation terminated..." severity failure;
	end process;
end behav;
