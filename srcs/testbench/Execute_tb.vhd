----------------------------------------------------------------------------------
-- Company: Politecnico di Milano
-- Engineer: Alessandro Comodi
-- Engineer: Davide Conficconi
-- 
-- Create Date: 03/21/2017 03:56:02 PM
-- Design Name: TiReX - single core
-- Module Name: Execute_tb - behav
-- Project Name: TiReX
-- Target Devices: Virtex 7 series
-- Tool Versions: Vivado 16.4
-- Description: This testbench verifies whether the Execute module behaves correctly
--				A series of tests check all the possible situations in which the
-- 				execution stage could be
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.std_logic_unsigned.all;

entity Execute_tb is
end Execute_tb;

architecture behav of Execute_tb is
	constant NCluster				: positive := 4;
	constant ClusterWidth			: positive := 4;
	constant DataWidth				: positive := 8;
	constant InternalBusWidthData	: positive := DataWidth * (NCluster + ClusterWidth - 1);
	constant AddressWidthData		: positive := 6;
	constant InternalOpBus			: positive := 2;

	-- input signals
	signal clk				: std_logic := '0';
	signal rst				: std_logic := '1';
	signal en_comparators	: std_logic_vector(NCluster - 1 downto 0) := (others => '0');
	signal data				: std_logic_vector(InternalBusWidthData - 1 downto 0) := (others => '0');
	signal reference 		: std_logic_vector(ClusterWidth * DataWidth - 1 downto 0) := (others => '0');
	signal valid_ref		: std_logic_vector(ClusterWidth - 1 downto 0) := (others => '0');
	signal operator			: std_logic_vector(InternalOpBus - 1 downto 0) := (others => '0'); 
	signal stall			: std_logic := '0';
	-- output signals
	signal data_offset		: std_logic_vector(AddressWidthData - 1 downto 0);
	signal match			: std_logic;
	signal vr_bin			: std_logic_vector(ClusterWidth - 1 downto 0);
	signal cluster_match	: std_logic_vector(NCluster - 1 downto 0);

begin
	EXECUTE : entity work.Execute
		generic map(
			InternalOpBus		=> InternalOpBus,
			ClusterWidth		=> ClusterWidth,
			NCluster			=> NCluster,
			DataWidth			=> DataWidth,
			AddressWidthData	=> AddressWidthData,
			BusWidthData		=> InternalBusWidthData
			)
		port map(
			clk					=> clk,
			rst					=> rst,
			en_comparators		=> en_comparators,
			data 				=> data,
			reference			=> reference,
			valid_ref			=> valid_ref,
			operator			=> operator,
			data_offset			=> data_offset,
			match				=> match,
			cluster_match		=> cluster_match,
			vr_bin				=> vr_bin,
			stall				=> stall
			);

		clk <= not clk after 5 ns;

		process 
		begin
			rst <= '1';

			wait until rising_edge(clk);

			-- This test checks whether, if there is an OR and the third cluster
			-- performs the match.
			data 		<= std_logic_vector(to_unsigned(character'pos('A'), 8)) &
							std_logic_vector(to_unsigned(character'pos('B'), 8)) &
							std_logic_vector(to_unsigned(character'pos('A'), 8)) &
							std_logic_vector(to_unsigned(character'pos('A'), 8)) &
							std_logic_vector(to_unsigned(character'pos('A'), 8)) &
							std_logic_vector(to_unsigned(character'pos('A'), 8)) &
							std_logic_vector(to_unsigned(character'pos('A'), 8));

			reference	<= std_logic_vector(to_unsigned(character'pos('C'), 8)) &
							std_logic_vector(to_unsigned(character'pos('B'), 8)) &
							std_logic_vector(to_unsigned(character'pos('D'), 8)) &
							std_logic_vector(to_unsigned(character'pos('F'), 8));

			valid_ref 	<= "1111";
			operator	<= "01";

			wait until falling_edge(clk);

			assert 	match = '1' and
					data_offset = std_logic_vector(to_unsigned(1, AddressWidthData)) and
					vr_bin = std_logic_vector(to_unsigned(1, ClusterWidth)) and
					cluster_match = "1011"
				report "First test failed." severity failure;

			wait until rising_edge(clk);

			-- This test checks whether the execute performs the match of the 
			-- fourth cluster and the valid reference is 2 instead of 4
			data        <= std_logic_vector(to_unsigned(character'pos('B'), 8)) &
                            std_logic_vector(to_unsigned(character'pos('D'), 8)) &
                            std_logic_vector(to_unsigned(character'pos('A'), 8)) &
                            std_logic_vector(to_unsigned(character'pos('A'), 8)) &
                            std_logic_vector(to_unsigned(character'pos('D'), 8)) &
                            std_logic_vector(to_unsigned(character'pos('A'), 8)) &
                            std_logic_vector(to_unsigned(character'pos('A'), 8));

            reference   <= std_logic_vector(to_unsigned(character'pos('A'), 8)) &
                            std_logic_vector(to_unsigned(character'pos('D'), 8)) &
                            std_logic_vector(to_unsigned(0, 8)) &
                            std_logic_vector(to_unsigned(0, 8));

            valid_ref   <= "1100";
            operator    <= "10";

			wait until falling_edge(clk);

			assert 	match = '1' and
					data_offset = std_logic_vector(to_unsigned(2, AddressWidthData)) and
					vr_bin = std_logic_vector(to_unsigned(2, ClusterWidth)) and
					cluster_match = "1110"
				report "Second test failed." severity failure;

			wait until rising_edge(clk);

			-- This test checks if, even if the reference could match with the data in input
			-- the valid reference takes as valid only the first character which doesn't match
			data        <= std_logic_vector(to_unsigned(character'pos('f'), 8)) &
	                            std_logic_vector(to_unsigned(character'pos('e'), 8)) &
	                            std_logic_vector(to_unsigned(character'pos('A'), 8)) &
	                            std_logic_vector(to_unsigned(character'pos('A'), 8)) &
	                            std_logic_vector(to_unsigned(character'pos('A'), 8)) &
	                            std_logic_vector(to_unsigned(character'pos('A'), 8)) &                
	                            std_logic_vector(to_unsigned(character'pos('A'), 8));

	        reference   <= std_logic_vector(to_unsigned(character'pos('s'), 8)) &
	                        std_logic_vector(to_unsigned(character'pos('f'), 8)) &
	                        std_logic_vector(to_unsigned(0, 8)) &
	                        std_logic_vector(to_unsigned(0, 8));

	        valid_ref   <= "1000";
	        operator    <= "01";

			wait until falling_edge(clk);

			assert 	match = '0' and
					data_offset = std_logic_vector(to_unsigned(4, AddressWidthData)) and
					vr_bin = std_logic_vector(to_unsigned(1, ClusterWidth)) and
					cluster_match = "1111"
				report "Third test failed." severity failure;

			wait until rising_edge(clk);

			-- This test checks if the second cluster performs the match
			data        <= std_logic_vector(to_unsigned(character'pos('k'), 8)) &
                            std_logic_vector(to_unsigned(character'pos('g'), 8)) &
                            std_logic_vector(to_unsigned(character'pos('A'), 8)) &
                            std_logic_vector(to_unsigned(character'pos('A'), 8)) &
                            std_logic_vector(to_unsigned(character'pos('A'), 8)) &
                            std_logic_vector(to_unsigned(character'pos('A'), 8)) &                            
                            std_logic_vector(to_unsigned(character'pos('A'), 8));

            reference   <= std_logic_vector(to_unsigned(character'pos('g'), 8)) &
                            std_logic_vector(to_unsigned(character'pos('A'), 8)) &
                            std_logic_vector(to_unsigned(character'pos('A'), 8)) &            
                            std_logic_vector(to_unsigned(character'pos('A'), 8));

            valid_ref   <= "1111";
            operator    <= "10";

			wait until falling_edge(clk);

			assert 	match = '1' and
					data_offset = std_logic_vector(to_unsigned(4, AddressWidthData)) and
					vr_bin = std_logic_vector(to_unsigned(4, ClusterWidth)) and
					cluster_match = "1011"
				report "Fourth test failed." severity failure;

			wait until rising_edge(clk);

			-- This test checks if the cluster that performs the match is the first one
			-- even if the match is found also in the third cluster.
			data        <= std_logic_vector(to_unsigned(character'pos('f'), 8)) &
                            std_logic_vector(to_unsigned(character'pos('e'), 8)) &
                            std_logic_vector(to_unsigned(character'pos('f'), 8)) &
                            std_logic_vector(to_unsigned(character'pos('e'), 8)) &
                            std_logic_vector(to_unsigned(character'pos('f'), 8)) &
                            std_logic_vector(to_unsigned(character'pos('e'), 8)) &
                            std_logic_vector(to_unsigned(character'pos('A'), 8));

            wait until rising_edge(clk);

            reference   <= std_logic_vector(to_unsigned(character'pos('f'), 8)) &
                            std_logic_vector(to_unsigned(character'pos('e'), 8)) &
                            std_logic_vector(to_unsigned(character'pos('f'), 8)) &
                            std_logic_vector(to_unsigned(character'pos('e'), 8));

            valid_ref   <= "1111";
            operator    <= "10";

            wait until falling_edge(clk);

			assert 	match = '1' and
					data_offset = std_logic_vector(to_unsigned(4, AddressWidthData)) and
					vr_bin = std_logic_vector(to_unsigned(4, ClusterWidth)) and
					cluster_match = "0111"
				report "Fifth test failed." severity failure;            

			wait until rising_edge(clk);

			-- This test checks if the execute does not perform the match even if there is
			-- one in the case the comparators are disabled. (Comparators are disabled if 
			-- "enable_comparators" has high values)
            data        <= std_logic_vector(to_unsigned(character'pos('A'), 8)) &
                            std_logic_vector(to_unsigned(character'pos('B'), 8)) &
                            std_logic_vector(to_unsigned(character'pos('A'), 8)) &
                            std_logic_vector(to_unsigned(character'pos('A'), 8)) &
                            std_logic_vector(to_unsigned(character'pos('A'), 8)) &
                            std_logic_vector(to_unsigned(character'pos('A'), 8)) &                           
                            std_logic_vector(to_unsigned(character'pos('A'), 8));

            reference   <= std_logic_vector(to_unsigned(character'pos('A'), 8)) &
                            std_logic_vector(to_unsigned(character'pos('B'), 8)) &
                            std_logic_vector(to_unsigned(character'pos('A'), 8)) &
                            std_logic_vector(to_unsigned(character'pos('A'), 8));

            valid_ref   	<= "1100";
            operator    	<= "10";
            en_comparators 	<= "1111";

            wait until falling_edge(clk);

			assert 	match = '0' and
					data_offset = std_logic_vector(to_unsigned(4, AddressWidthData)) and
					vr_bin = std_logic_vector(to_unsigned(2, ClusterWidth)) and
					cluster_match = "1111"
				report "Sixth test failed." severity failure;            

			wait until rising_edge(clk);

			-- This test checks whether the second cluster performs the match
            data        <= std_logic_vector(to_unsigned(character'pos('A'), 8)) &
                            std_logic_vector(to_unsigned(character'pos('B'), 8)) &
                            std_logic_vector(to_unsigned(character'pos('A'), 8)) &
                            std_logic_vector(to_unsigned(character'pos('A'), 8)) &
                            std_logic_vector(to_unsigned(character'pos('A'), 8)) &
                            std_logic_vector(to_unsigned(character'pos('A'), 8)) &                           
                            std_logic_vector(to_unsigned(character'pos('A'), 8));

            reference   <= std_logic_vector(to_unsigned(character'pos('D'), 8)) &
                            std_logic_vector(to_unsigned(character'pos('B'), 8)) &
                            std_logic_vector(to_unsigned(character'pos('F'), 8)) &
                            std_logic_vector(to_unsigned(character'pos('R'), 8));

            valid_ref   	<= "1111";
            operator    	<= "01";
            en_comparators 	<= "0000";

            wait until falling_edge(clk);

			assert 	match = '1' and
					data_offset = std_logic_vector(to_unsigned(1, AddressWidthData)) and
					vr_bin = std_logic_vector(to_unsigned(1, ClusterWidth)) and
					cluster_match = "1011"
				report "Seventh test failed." severity failure;            

			wait until rising_edge(clk);

			-- This test checks if the execute behaves correctly whenever there is 
			-- an HIGH stall signal
            data        <=   std_logic_vector(to_unsigned(character'pos('A'), 8)) &
                            std_logic_vector(to_unsigned(character'pos('B'), 8)) &
                            std_logic_vector(to_unsigned(character'pos('A'), 8)) &
                            std_logic_vector(to_unsigned(character'pos('A'), 8)) &
                            std_logic_vector(to_unsigned(character'pos('A'), 8)) &
                            std_logic_vector(to_unsigned(character'pos('A'), 8)) &                           
                            std_logic_vector(to_unsigned(character'pos('A'), 8));

            reference   <= std_logic_vector(to_unsigned(character'pos('D'), 8)) &
                            std_logic_vector(to_unsigned(character'pos('B'), 8)) &
                            std_logic_vector(to_unsigned(character'pos('F'), 8)) &
                            std_logic_vector(to_unsigned(character'pos('R'), 8));

            valid_ref   	<= "1111";
            operator    	<= "01";
            stall			<= '1';

            wait until falling_edge(clk);

			assert 	match = '0' and
					data_offset = std_logic_vector(to_unsigned(4, AddressWidthData)) and
					vr_bin = std_logic_vector(to_unsigned(1, ClusterWidth)) and
					cluster_match = "1111"
				report "Eight test failed." severity failure;            

			wait until rising_edge(clk);

			assert false report "Testbench successful... terminating execution" severity failure;
			wait;
		end process;	

end behav;
