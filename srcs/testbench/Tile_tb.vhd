----------------------------------------------------------------------------------
-- Company: Politecnico di Milano 
-- Student: Alessandro Comodi
-- 
-- Create Date: 04/13/2017 10:55:53 AM
-- Design Name: 
-- Module Name: Tile_tb - behav
-- Project Name: TiReX
-- Target Devices: teVirtex 7 - VC707 
-- Tool Versions: Vivado 2016.4
-- Description: This is the testbench file that checks the behaviour of the complete system
--              The test takes in input files containing Instructions and Data to be analyzed.
--              First of all the instructions are loaded in the Instruction Memory.
--              Then a loop analyzes and loads the data from the Data files when the core
--              is in need of new data.
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.std_logic_unsigned.all;
use work.GenericsPack.all;
use ieee.std_logic_textio.all;
use std.textio.all;

entity Tile_tb is
generic(
    -- This generics must be defined from Vivado in 'Simulation Settings'
    DataInPath              : string;
    InstructionInPath       : string;
    ClusterWidth            : positive;
    NCluster                : positive
    );
end Tile_tb;

architecture behav of Tile_tb is

    constant InternalBusWidthData       : positive := RamWidthData * (NCluster + ClusterWidth - 1);
    constant RamWidthInstr      : positive := ClusterWidth * DataWidth + OpCodeWidth;
    constant padding            : std_logic_vector(ClusterWidth * DataWidth - 1 downto 0) := (others => '0');

    -- Control Signals
    signal clk                  : std_logic := '0';
    signal rst                  : std_logic := '0';
    signal addr_start_instr     : std_logic_vector(AddressWidthInstr - 1 downto 0) := (others => '0');
    signal src_en               : std_logic := '0';
    signal complete             : std_logic;
    signal found                : std_logic;

    signal data_in_data_mem     : std_logic_vector(ExternalBusWidthData - 1 downto 0) := (others => '0');
    signal data_in_instr_mem    : std_logic_vector(ExternalBusWidthInstr - 1 downto 0) := (others => '0');
    signal addr_instr_mem       : std_logic_vector(AddressWidthInstr - 1 downto 0) := (others => '0');
    signal addr_data_mem        : std_logic_vector(AddressWidthData - 1 downto 0) := (others => '0');

    signal first_character      : std_logic_vector(CharacterNumber - 1 downto 0) := (others => '0');
    signal ending_character     : std_logic_vector(CharacterNumber - 1 downto 0) := (others => '1');

    signal curr_character       : std_logic_vector(CharacterNumber - 1 downto 0);
    signal curr_character_check : std_logic_vector(CharacterNumber - 1 downto 0) := (others => '0');
    signal curr_last_match_char : std_logic_vector(CharacterNumber - 1 downto 0);
    signal reload_bram          : std_logic_vector(1 downto 0);
    signal bram_addr            : std_logic_vector(AddrWidthDataBRAM - 1 downto 0) := (others => '0');
    signal control              : std_logic := '0';
    signal reload               : std_logic := '0';
    signal backup_data          : std_logic_vector(ExternalBusWidthData - 1 downto 0) := (others => '0');
    signal difference           : std_logic_vector(CharacterNumber - 1 downto 0) := (others => '0');
    signal we_instr_data        : std_logic := '0';
    signal we_instr_opcode      : std_logic := '0';
    signal we_data              : std_logic := '0';

    file data_input_file        : text;
    file instr_input_file       : text;

    signal data_ram_loaded      : std_logic := '0';
    signal instr_ram_loaded     : std_logic := '0';

    procedure verifyOpenFileState (
        variable state : in  file_open_status;
        variable isOk  : out boolean) is
    begin
        case state is
            when open_ok => isOk := true;
            when others  => isOk := false;
        end case;
    end procedure verifyOpenFileState;

begin
    TIREX_CORE  : entity work.Tile
        generic map(
            AddressWidthInstr       => AddressWidthInstr,
            AddressWidthData        => AddressWidthData,
            CharacterNumber         => CharacterNumber,
            OpCodeBus               => OpCodeBus,
            OpCodeWidth             => OpCodeWidth,
            InternalOpBus           => InternalOpBus,
            DataWidth               => DataWidth,
            ClusterWidth            => ClusterWidth,
            NCluster                => NCluster,
            ExternalBusWidthData    => ExternalBusWidthData,
            InternalBusWidthData    => InternalBusWidthData,

            BufferAddressWidth      => BufferAddressWidth,
            StackDataWidth          => StackDataWidth,

            ExternalBusWidthInstr   => ExternalBusWidthInstr,
            RamWidthInstr           => RamWidthInstr,
            RamWidthData            => RamWidthData
            )
        port map(
            clk                 => clk,
            rst                 => rst,
            addr_start_instr    => addr_start_instr,
            first_character     => first_character,
            ending_character    => ending_character,

            curr_character      => curr_character,
            curr_last_match_char=> curr_last_match_char,
            reload_bram         => reload_bram,
            reload_complete     => reload,

            src_en              => src_en,
            complete            => complete,
            found               => found,
            instr_address_wr    => addr_instr_mem,
            instr_data_in       => data_in_instr_mem,
            instr_we_data       => we_instr_data,
            instr_we_opcode     => we_instr_opcode,
            data_address_wr     => addr_data_mem,
            data_data_in        => data_in_data_mem,
            data_we             => we_data
            );

    clk <= not clk after 5 ns;

    functional_testbench : process
        type char_array is array (1 to ExternalBusWidthData / RamWidthData) of character;
        -- data variables
        variable stim_line        : line;   -- line from file
        variable good             : boolean;
        variable rd_ch            : char_array;  -- line of memory (ASCII characters)
        variable rd_ch_backup     : char_array := "BACKUPARRAYMATCH";
        variable st               : file_open_status;    

        --instruction variables
        variable stim_line_instr  : line;   -- line from file
        variable good_instr       : boolean;
        variable rd_line_instr    : std_logic_vector(ExternalBusWidthInstr - 1 downto 0);
        variable st_instr         : file_open_status;
    begin
        -- This loop is needed in roder to repeat the test a number N of times.
        -- We repeat the test 2 times.
        for j in 0 to 1 loop
            rst                     <= '0';
            src_en                  <= '0';
            addr_instr_mem          <= (others => '0');
            curr_character_check    <= (others => '0');
            bram_addr               <= (others => '0');
            
            wait until rising_edge(clk);
        
            rst              <= '1';
            instr_ram_loaded <= '0';
            
            ------------- Starting Initialization of Instruction Memory -------------
            file_open(st_instr, instr_input_file, InstructionInPath, read_mode);
            verifyOpenFileState(st_instr, good_instr);
            assert good_instr = true
              report InstructionInPath & " not found. Simulation aborted." severity failure;    

            -- start loading data from file
            while not endfile(instr_input_file) loop
                we_instr_data       <= '1';
                we_instr_opcode     <= '0';
                readline(instr_input_file, stim_line_instr);
                read(stim_line_instr, rd_line_instr, good);

                data_in_instr_mem   <= rd_line_instr;     
                -- synchronize on clock signal the writing process
                wait until rising_edge(clk);

                we_instr_data       <= '0';
                we_instr_opcode     <= '1';
                readline(instr_input_file, stim_line_instr);
                read(stim_line_instr, rd_line_instr, good);
                data_in_instr_mem   <= rd_line_instr;

                wait until rising_edge(clk);

                addr_instr_mem  <= addr_instr_mem + 1;
            end loop;

            -- disable writing
            we_instr_data       <= '0';
            we_instr_opcode     <= '0';
            -- close input file
            file_close(instr_input_file);
            instr_ram_loaded    <= '1';
            --------------------- Instruction Memory Loaded -----------------------

            ------------------------ Starting the Search -------------------------
            src_en              <= '1';
            file_open(st, data_input_file, DataInPath, read_mode);
            verifyOpenFileState(st, good);
            assert good = true
              report DataInPath & " not found. Simulation aborted." severity failure;    
            
            addr_start_instr    <= (others => '0');
            first_character     <= (others => '0');
            ending_character    <= "11111111111100";
            addr_data_mem       <= (others => '0');
            bram_addr           <= (others => '0');

            -- This loop is needed to load data into the Data Memory of the core
            -- whenever it is needed
            while (complete = '0' or not endfile(data_input_file)) loop
                -- When a match is found the backup data is saved in order to restore
                -- it if the match failes at some point
                if reload_bram = "10" then
                    for i in 1 to ExternalBusWidthData / RamWidthData loop
                        backup_data(ExternalBusWidthData - (i - 1) * RamWidthData - 1 downto ExternalBusWidthData - i * RamWidthData) <= 
                                                std_logic_vector(to_unsigned(character'pos(rd_ch_backup(i)), DataWidth)); 
                    end loop;
                end if;
                
                -- When reload_bram is "01" it is needed to reload the Data Memory from
                -- the BRAM. This portion of code is needed to set the core in order to
                -- let it accept backup data from the BRAM
                if reload_bram = "01" then
                    bram_addr           <= curr_last_match_char(CharacterNumber - 1 downto CharacterNumber - AddrWidthDataBRAM) + 1;
                    reload              <= '1';
                    addr_data_mem       <= (others => '0');
                    data_in_data_mem    <= backup_data;
                    we_data             <= '1';
                    wait until rising_edge(clk);
                    we_data         <= '0';
                    addr_data_mem   <= addr_data_mem + ExternalBusWidthData / RamWidthData;
                
                -- This branch of the if is needed to simulate the BRAM delay in retrieving
                -- the backup data
                elsif reload = '1' then
                    wait until rising_edge(clk);
                    we_data <= '1';
                    control <= '1';
                    wait until rising_edge(clk);
                    we_data                 <= '0';
                    reload                  <= '0';
                    control                 <= '0';
                    addr_data_mem           <= addr_data_mem + ExternalBusWidthData / RamWidthData;
                    curr_character_check    <= bram_addr & "0000";

                -- Whenever the condition is true the Core needs to update its Data Memory
                -- and so a new line from the file it's read and this branch simulates also
                -- the deleay apported by the BRAM
                elsif curr_character_check - curr_character <= (NCluster + ClusterWidth - 1) * 2 then
                    curr_character_check    <= curr_character_check + ExternalBusWidthData / DataWidth;
                    control                 <= '1';
                    we_data                 <= '1';
                    readline(data_input_file, stim_line);

                    for i in 1 to ExternalBusWidthData / RamWidthData loop
                        read(stim_line, rd_ch(i));
                    end loop;
                  
                    for i in 1 to ExternalBusWidthData / RamWidthData loop
                        data_in_data_mem(ExternalBusWidthData - (i - 1) * RamWidthData - 1 downto ExternalBusWidthData - i * RamWidthData) <= 
                                                std_logic_vector(to_unsigned(character'pos(rd_ch(i)), DataWidth)); 
                    end loop;

                    wait until rising_edge(clk);
                    control       <= '0';
                    we_data       <= '0';
                    addr_data_mem <= addr_data_mem + ExternalBusWidthData / RamWidthData;
                    bram_addr     <= bram_addr + 2;
                end if;
                wait until rising_edge(clk);
            end loop;
            file_close(data_input_file);
            -------------------------- Ending the Search ---------------------------

            -- This loop is needed to easily check in the waveform graph when the 
            -- search iteration has been completed and the following one starts
            -- again
            for i in 0 to 10 loop
                wait until rising_edge(clk);
            end loop;
        end loop;

        rst <= '0';
        wait;
    end process functional_testbench;
end behav;