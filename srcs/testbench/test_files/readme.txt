Set the generics/parameters in vivado
DataInPath: path to the ram data file
InstructionInPath: path to the instruction ram file
ClusterWidth: the number of comparators per clusters
NCluster: the number of clusters of comparators

-- 2 Comparators		          Has to be found
(ClusterWidth = 2, NCluster = 2)
Test0	: A|B			          			YES 		
Test1	: AB(A|B)		          			YES
Test2	: (A|B)AB		          			YES
Test3	: ABABABAB		          			YES
Test4	: (A|B)AB(A|B)ABAB	      			NO
Test5	: AB(A|B)(A|B)AB	      			YES
Test6	: CCAB(A|B)BA		      			YES
Test7	: AB(A|B)AB(A|B)	      			NO
Test15  : CCCC(C|A)(C|B)CCEG	      		YES
Test16  : GHEF(A|B)(A|B)RRTY 	      		YES

-- 4 Comparators		
(ClusterWidth = 4, NCluster = 4)
Test8	: ABC(A|B|C)		      			YES
Test9	: ABCD(A|B|C|D)		      			YES
Test10	: (A|D|C)ABCD(A|B)	      			YES
Test11  : (A|B|C|D)ABCDEF(A|C|D)  			YES
Test12  : (A|B|C|D)ABCDEF(A|C|D)  			NO
Test13  : (A|B|C|D)ABCDEF(A|C|D)  			YES

Up to here the parenthesis are just for human comprehension

--Parenthesis Tests 4 Comparators
Test17 	: (ABCD)		  					YES
Test18 	: (DDDD) 		  					NO
Test19	: ABC(DDDD)		  					YES
Test20	: (DDDD)(A|B|C|D)	  				NO
Test20b	: (DDDD)(A|B|C|D) 	  				YES

--Kleene Operators Tests 4 Comparators
Test21 	: (BB)+			  					YES
Test22	: (CDBA)+		  					YES
Test23	: DDEEFG(GJ)+		  				YES
(Switching data between 22 and 23 you can see how it works if you don't want to match the RE)

--Jump Operator Tests 4 Comparators
Test24	: (BBCD)|(CCDD)|(EFGH)				YES
Test25	: (AAAA)|(CCCC)|(BBBB)|(DDDD)EEEE 	YES


--Flex vs TiReX on chromosome file
TestF1 : (TTTT)+CT							YES
TestF2 : ACCGTGGA							YES
TestF3 : (CAGT)|(GGGG)|(TTGG)TGCA(C|G)+		YES
TestF4 : ACA(CAGT)|(GGGG)|(TTGG)TGCA(C|G)+	YES
