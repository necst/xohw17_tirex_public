----------------------------------------------------------------------------------
-- Company: Politecnico di Milano
-- Engineer: Alessandro Comodi
-- Engineer: Davide Conficconi
-- 
-- Create Date: 03/21/2017 03:56:02 PM
-- Design Name: TiReX - single core
-- Module Name: FetchDecode_tb - behav
-- Project Name: TiReX
-- Target Devices: Virtex 7 series
-- Tool Versions: Vivado 16.4
-- Description: This test checks the correct behaviour of the Fetch and Decode
-- 				component
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use ieee.numeric_std.all;

entity FetchDecode_tb is
end FetchDecode_tb;

architecture behav_test1 of FetchDecode_tb is

	-- Constants needed for the Fetch and Decode generics
	constant ClusterWidth		: positive := 4;
	constant DataWidth			: positive := 8;
	constant OpCodeBus			: positive := 10;
	constant OpCodeWidth		: positive := 6;
	
	-- internal signals
	signal clk 				: std_logic := '0';

	signal instruction_i	: std_logic_vector((DataWidth * ClusterWidth + OpCodeWidth) - 1 downto 0);
	signal op_code_out_i	: std_logic_vector(OpCodeBus - 1 downto 0);
	signal valid_ref_i		: std_logic_vector(ClusterWidth - 1 downto 0);
	signal instr_data_i		: std_logic_vector((DataWidth * ClusterWidth) - 1 downto 0);

begin
	FetchDecode_DUT : entity work.FetchDecode
		generic map(
			ClusterWidth	=> ClusterWidth,
			OpCodeBus		=> OpCodeBus,
			OpCodeWidth		=> OpCodeWidth,
			DataWidth		=> DataWidth)
		port map(
			instruction 	=> instruction_i,
			op_code_out 	=> op_code_out_i,
			valid_ref 		=> valid_ref_i,
			instr_data 		=> instr_data_i);

	clk <= not clk after 5 ns;

	process 
	begin
		wait until rising_edge(clk);

		-- This test checks if the jump instruction is decoded
		instruction_i 	<= "000111" &
							std_logic_vector(to_unsigned(0, 8)) &
							std_logic_vector(to_unsigned(0, 8)) &
							std_logic_vector(to_unsigned(0, 8)) &
							std_logic_vector(to_unsigned(0, 8));

		wait until falling_edge(clk);

		assert 	instr_data_i	= 0 and
				op_code_out_i	= "1000000000" and
				valid_ref_i		= "0000"
			report "First test failed." severity failure;

		wait until rising_edge(clk);

		-- This test checks if the open parenthesis is correctly decoded
		instruction_i 	<= "100000" &
							std_logic_vector(to_unsigned(0, 8)) &
							std_logic_vector(to_unsigned(0, 8)) &
							std_logic_vector(to_unsigned(0, 8)) &
							std_logic_vector(to_unsigned(0, 8));

		wait until falling_edge(clk);

		assert 	instr_data_i	= 0 and
				op_code_out_i	= "0001000000" and
				valid_ref_i		= "0000"
			report "Second test failed." severity failure;

		wait until rising_edge(clk);

		-- This test checks if the NOP instruction is correctly decoded
		instruction_i 	<= "000000" &
							std_logic_vector(to_unsigned(0, 8)) &
							std_logic_vector(to_unsigned(0, 8)) &
							std_logic_vector(to_unsigned(0, 8)) &
							std_logic_vector(to_unsigned(0, 8));

		wait until falling_edge(clk);

		assert 	instr_data_i	= 0 and
				op_code_out_i	= "0000000100" and
				valid_ref_i		= "0000"
			report "Third test failed." severity failure;

		wait until rising_edge(clk);

		-- This test checks wheter the Always instruction is correctly decoded
		instruction_i 	<= "011000" &
							std_logic_vector(to_unsigned(0, 8)) &
							std_logic_vector(to_unsigned(0, 8)) &
							std_logic_vector(to_unsigned(0, 8)) &
							std_logic_vector(to_unsigned(0, 8));

		wait until falling_edge(clk);

		assert 	instr_data_i	= 0 and
				op_code_out_i	= "0100000000" and
				valid_ref_i		= "0000"
			report "Fourth test failed." severity failure;

		wait until rising_edge(clk);

		-- This test checks if the OR instruction is decoded and the value of 
		-- valid_reference is correct
		instruction_i 	<= "001000" &
							std_logic_vector(to_unsigned(character'pos('A'), 8)) &
							std_logic_vector(to_unsigned(character'pos('D'), 8)) &
							std_logic_vector(to_unsigned(character'pos('B'), 8)) &
							std_logic_vector(to_unsigned(0, 8));

		wait until falling_edge(clk);

		assert 	instr_data_i	= std_logic_vector(to_unsigned(character'pos('A'), 8)) &
									std_logic_vector(to_unsigned(character'pos('D'), 8)) &
									std_logic_vector(to_unsigned(character'pos('B'), 8)) &
									std_logic_vector(to_unsigned(0, 8)) and
				op_code_out_i	= "0000000001" and
				valid_ref_i		= "1110"
			report "Fifth test failed." severity failure;

		wait until rising_edge(clk);

		-- This test checks if the AND instruction is decoded and the value of
		-- valid_reference is correct
		instruction_i 	<= "010000" &
							std_logic_vector(to_unsigned(character'pos('A'), 8)) &
							std_logic_vector(to_unsigned(character'pos('D'), 8)) &
							std_logic_vector(to_unsigned(character'pos('B'), 8)) &
							std_logic_vector(to_unsigned(character'pos('F'), 8));

		wait until falling_edge(clk);

		assert 	instr_data_i	= std_logic_vector(to_unsigned(character'pos('A'), 8)) &
									std_logic_vector(to_unsigned(character'pos('D'), 8)) &
									std_logic_vector(to_unsigned(character'pos('B'), 8)) &
									std_logic_vector(to_unsigned(character'pos('F'), 8)) and
				op_code_out_i	= "0000000010" and
				valid_ref_i		= "1111"
			report "Sixth test failed." severity failure;

		wait until rising_edge(clk);

		-- This test checks if the OR and "Kleene plus" instruction is correctly decoded
		instruction_i 	<= "001010" &
							std_logic_vector(to_unsigned(character'pos('A'), 8)) &
							std_logic_vector(to_unsigned(character'pos('D'), 8)) &
							std_logic_vector(to_unsigned(character'pos('B'), 8)) &
							std_logic_vector(to_unsigned(0, 8));

		wait until falling_edge(clk);

		assert 	instr_data_i	= std_logic_vector(to_unsigned(character'pos('A'), 8)) &
									std_logic_vector(to_unsigned(character'pos('D'), 8)) &
									std_logic_vector(to_unsigned(character'pos('B'), 8)) &
									std_logic_vector(to_unsigned(0, 8)) and
				op_code_out_i	= "0000001001" and
				valid_ref_i		= "1110"
			report "Seventh test failed." severity failure;

		wait until rising_edge(clk);

		-- This test checks if the AND and "Kleene plus" instruction is correctly decoded
		instruction_i 	<= "010010" &
							std_logic_vector(to_unsigned(character'pos('A'), 8)) &
							std_logic_vector(to_unsigned(character'pos('D'), 8)) &
							std_logic_vector(to_unsigned(0, 8)) &
							std_logic_vector(to_unsigned(0, 8));

		wait until falling_edge(clk);

		assert 	instr_data_i	= std_logic_vector(to_unsigned(character'pos('A'), 8)) &
									std_logic_vector(to_unsigned(character'pos('D'), 8)) &
									std_logic_vector(to_unsigned(0, 8)) &
									std_logic_vector(to_unsigned(0, 8)) and
				op_code_out_i	= "0000001010" and
				valid_ref_i		= "1100"
			report "Eight test failed." severity failure;

		wait until rising_edge(clk);

		-- This test checks if the OR and "Kleene star" instruction is correctly decoded
		instruction_i 	<= "001001" &
							std_logic_vector(to_unsigned(character'pos('A'), 8)) &
							std_logic_vector(to_unsigned(character'pos('D'), 8)) &
							std_logic_vector(to_unsigned(character'pos('B'), 8)) &
							std_logic_vector(to_unsigned(0, 8));

		wait until falling_edge(clk);

		assert 	instr_data_i	= std_logic_vector(to_unsigned(character'pos('A'), 8)) &
									std_logic_vector(to_unsigned(character'pos('D'), 8)) &
									std_logic_vector(to_unsigned(character'pos('B'), 8)) &
									std_logic_vector(to_unsigned(0, 8)) and
				op_code_out_i	= "0000010001" and
				valid_ref_i		= "1110"
			report "Ninth test failed." severity failure;

		wait until rising_edge(clk);

		-- This test checks if the AND and "Kleene star" instruction is correctly decoded
		instruction_i 	<= "010001" &
							std_logic_vector(to_unsigned(character'pos('A'), 8)) &
							std_logic_vector(to_unsigned(character'pos('D'), 8)) &
							std_logic_vector(to_unsigned(character'pos('B'), 8)) &
							std_logic_vector(to_unsigned(0, 8));

		wait until falling_edge(clk);

		assert 	instr_data_i	= std_logic_vector(to_unsigned(character'pos('A'), 8)) &
									std_logic_vector(to_unsigned(character'pos('D'), 8)) &
									std_logic_vector(to_unsigned(character'pos('B'), 8)) &
									std_logic_vector(to_unsigned(0, 8)) and
				op_code_out_i	= "0000010010" and
				valid_ref_i		= "1110"
			report "Tenth test failed." severity failure;

		wait until rising_edge(clk);

		-- This test checks if the OR and "closed parenthesis OR" instruction is correctly decoded
		instruction_i 	<= "001011" &
							std_logic_vector(to_unsigned(character'pos('A'), 8)) &
							std_logic_vector(to_unsigned(0, 8)) &
							std_logic_vector(to_unsigned(0, 8)) &
							std_logic_vector(to_unsigned(0, 8));

		wait until falling_edge(clk);

		assert 	instr_data_i	= std_logic_vector(to_unsigned(character'pos('A'), 8)) &
									std_logic_vector(to_unsigned(0, 8)) &
									std_logic_vector(to_unsigned(0, 8)) &
									std_logic_vector(to_unsigned(0, 8)) and
				op_code_out_i	= "0000100001" and
				valid_ref_i		= "1000"
			report "Eleventh test failed." severity failure;

		wait until rising_edge(clk);

		-- This test checks if the AND and "closed parenthesis OR" instruction is correctly decoded
		instruction_i 	<= "010011" &
							std_logic_vector(to_unsigned(character'pos('A'), 8)) &
							std_logic_vector(to_unsigned(0, 8)) &
							std_logic_vector(to_unsigned(0, 8)) &
							std_logic_vector(to_unsigned(0, 8));

		wait until falling_edge(clk);

		assert 	instr_data_i	= std_logic_vector(to_unsigned(character'pos('A'), 8)) &
									std_logic_vector(to_unsigned(0, 8)) &
									std_logic_vector(to_unsigned(0, 8)) &
									std_logic_vector(to_unsigned(0, 8)) and
				op_code_out_i	= "0000100010" and
				valid_ref_i		= "1000"
			report "Twelveth test failed." severity failure;

		wait until rising_edge(clk);

		-- This test checks if the OR and "closed parenthesis" instruction is correctly decoded
		instruction_i 	<= "001100" &
							std_logic_vector(to_unsigned(character'pos('A'), 8)) &
							std_logic_vector(to_unsigned(character'pos('D'), 8)) &
							std_logic_vector(to_unsigned(0, 8)) &
							std_logic_vector(to_unsigned(0, 8));

		wait until falling_edge(clk);

		assert 	instr_data_i	= std_logic_vector(to_unsigned(character'pos('A'), 8)) &
									std_logic_vector(to_unsigned(character'pos('D'), 8)) &
									std_logic_vector(to_unsigned(0, 8)) &
									std_logic_vector(to_unsigned(0, 8)) and
				op_code_out_i	= "0010000001" and
				valid_ref_i		= "1100"
			report "Thirteenth test failed." severity failure;

		wait until rising_edge(clk);

		-- This test checks if the AND and "closed parenthesis" instruction is correctly decoded
		instruction_i 	<= "010100" &
							std_logic_vector(to_unsigned(character'pos('A'), 8)) &
							std_logic_vector(to_unsigned(character'pos('D'), 8)) &
							std_logic_vector(to_unsigned(0, 8)) &
							std_logic_vector(to_unsigned(0, 8));

		wait until falling_edge(clk);

		assert 	instr_data_i	= std_logic_vector(to_unsigned(character'pos('A'), 8)) &
									std_logic_vector(to_unsigned(character'pos('D'), 8)) &
									std_logic_vector(to_unsigned(0, 8)) &
									std_logic_vector(to_unsigned(0, 8)) and
				op_code_out_i	= "0010000010" and
				valid_ref_i		= "1100"
			report "Fourteenth test failed." severity failure;

		wait until rising_edge(clk);

		assert false report "Testbench successful... terminating execution" severity failure;
		wait;
	end process;
	

end behav_test1;
