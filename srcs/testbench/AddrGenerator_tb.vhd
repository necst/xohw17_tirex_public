----------------------------------------------------------------------------------
-- Company: Politecnico di Milano
-- Student: Alessandro Comodi
-- 
-- Create Date: 04/10/2017 04:01:57 PM
-- Design Name: TiReX
-- Module Name: AddrGenerator_tb - behav
-- Project Name: TiReX
-- Target Devices: Virtex 7 series
-- Tool Versions: Vivado 16.4
-- Description: This is the testbench for the Address Generator component
-- 
-- The test checks wether the component produces the right value of the
-- data_offset signal and for the cluster_match signals.
-- Note:    The cluster_match signal is a one-hot vector with a '0'
--          corresponding to the cluster that has performed the match.
--
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;

entity AddrGenerator_tb is
end AddrGenerator_tb;

architecture behav of AddrGenerator_tb is
    constant NCluster           : positive := 4;
    constant ClusterWidth       : positive := 4;
    constant AddressWidthData   : positive := 6;

    -- clock signal
    signal clk              : std_logic := '0';

    -- Control signals
    signal cluster_result   : std_logic_vector(NCluster - 1 downto 0) := (others => '0');
    signal valid_ref_bin    : std_logic_vector(ClusterWidth - 1 downto 0) := (others => '0');
    signal data_offset      : std_logic_vector(AddressWidthData - 1 downto 0);
    signal cluster_match    : std_logic_vector(NCluster - 1 downto 0);
begin
    
    ADDR_GEN    : entity work.AddrGenerator
        generic map(
            AddressWidthData    => AddressWidthData,
            NCluster            => NCluster,
            ClusterWidth        => ClusterWidth
            )
        port map(
            cluster_result      => cluster_result,
            valid_ref_bin       => valid_ref_bin,
            data_offset         => data_offset,
            cluster_match       => cluster_match
            );

    clk <= not clk after 5 ns;

    process
    begin
        wait until rising_edge(clk);

        cluster_result  <= "0100";
        valid_ref_bin   <= "0001";

        wait until falling_edge(clk);

        assert (data_offset = "000001") and (cluster_match = "1011") 
            report "First check failed!" severity failure;

        wait until rising_edge(clk);

        cluster_result  <= "0000";
        valid_ref_bin   <= "0011";

        wait until falling_edge(clk);

        assert data_offset = "000100" and cluster_match = "1111" 
            report "Second check failed!" severity failure;

        wait until rising_edge(clk);

        cluster_result  <= "1001";
        valid_ref_bin   <= "0010";

        wait until falling_edge(clk);

        assert data_offset = "000010" and cluster_match = "0111" 
            report "Third check failed!" severity failure;

        wait until rising_edge(clk);

        cluster_result  <= "1111";
        valid_ref_bin   <= "0001";

        wait until falling_edge(clk);

        assert data_offset = "000001" and cluster_match = "0111" 
            report "Fourth check failed!" severity failure;

        wait until rising_edge(clk);

        cluster_result  <= "0001";
        valid_ref_bin   <= "0100";

        wait until falling_edge(clk);

        assert data_offset = "000100" and cluster_match = "1110" 
            report "Fifth check failed!" severity failure;

        wait;
    end process;
end behav;
