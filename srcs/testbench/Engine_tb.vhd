----------------------------------------------------------------------------------
-- Company: Politecnico di Milano
-- Engineer: Alessandro Comodi
-- Engineer: Davide Conficconi
-- 
-- Create Date: 03/21/2017 03:56:02 PM
-- Design Name: TiReX - single core
-- Module Name: Engine_tb - behav
-- Project Name: TiReX
-- Target Devices: Virtex 7 series
-- Tool Versions: Vivado 16.4
-- Description: This testbench verifies the correctness of the Engine component.
--				There are 6 tests that checks all the possible situations.
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use ieee.numeric_std.all;

entity Engine_tb is
end Engine_tb;

architecture behav of Engine_tb is

	-- Constants needed for the Engine Generics
	constant NCluster 			: positive := 4;
	constant ClusterWidth 		: positive := 4;
	constant InternalOpBus		: positive := 2;
	constant AddressWidthData	: positive := 6;
	
	signal clk				: std_logic := '0';

	-- Control signals
	signal compare_results	: std_logic_vector(ClusterWidth * NCluster - 1 downto 0) := (others => '0');
	signal valid_ref		: std_logic_vector(ClusterWidth - 1 downto 0) := (others => '0');
	signal operator			: std_logic_vector(InternalOpBus - 1 downto 0) := (others => '0');
	signal address_offset	: std_logic_vector(AddressWidthData - 1 downto 0);
	signal match			: std_logic;
	signal vr_bin			: std_logic_vector(ClusterWidth - 1 downto 0);
	signal cluster_match	: std_logic_vector(NCluster	- 1 downto 0);
	signal stall			: std_logic := '0';
begin
	
	ENGINE : entity work.Engine 
		generic map(
			InternalOpBus		=> InternalOpBus,
			NCluster 			=> NCluster,
			ClusterWidth		=> ClusterWidth,
			AddressWidthData	=> AddressWidthData
			)
		port map(
			compare_results		=> compare_results,
			valid_ref			=> valid_ref,
			operator			=> operator,
			address_offset		=> address_offset,
			match 				=> match,
			vr_bin 				=> vr_bin,
			cluster_match		=> cluster_match,
			stall				=> stall
			);

	clk <= not clk after 5 ns;

	process
	begin
		wait until rising_edge(clk);

		-- This test checks if the last cluster performed the match
		-- and the right offset
		compare_results <= "0001001101111111";
		valid_ref		<= "1111";
		operator		<= "10";
		stall 			<= '0';

		wait until falling_edge(clk);

		assert 	address_offset	= std_logic_vector(to_unsigned(4, AddressWidthData)) and 
				match 			= '1' and
				vr_bin			= std_logic_vector(to_unsigned(4, ClusterWidth)) and
				cluster_match	= "1110" 
			report "First test failed. Wrong result analysis!" severity failure;		  			

		wait until rising_edge(clk);

		-- This test checks if the selected cluster is the first one and if 
		-- the address offset is set to 1 because the operator is an or.
		compare_results <= "1111111111111111";
		valid_ref		<= "1111";
		operator		<= "01";
		stall 			<= '0';

		wait until falling_edge(clk);

		assert 	address_offset	= std_logic_vector(to_unsigned(1, AddressWidthData)) and 
				match 			= '1' and
				vr_bin			= std_logic_vector(to_unsigned(1, ClusterWidth)) and
				cluster_match	= "0111" 
			report "Second test failed. Wrong result analysis!" severity failure;

		wait until rising_edge(clk);

		-- This test checks if the engine produced a not match signal (match = '0')
		-- because all the clusters didn't find any valid match. vr_bin must be 2
		compare_results <= "0010000001000010";
		valid_ref		<= "1100";
		operator		<= "10";
		stall 			<= '0';

		wait until falling_edge(clk);

		assert 	address_offset	= std_logic_vector(to_unsigned(4, AddressWidthData)) and 
				match 			= '0' and
				vr_bin			= std_logic_vector(to_unsigned(2, ClusterWidth)) and
				cluster_match	= "1111" 
			report "Third test failed. Wrong result analysis!" severity failure;

		wait until rising_edge(clk);

		-- This test verifies wether the address_offset does not change if there is a stall
		-- in the pipeline. cluster_match must be full of '1' and match must be '0';
		compare_results <= "0011100011001100";
		valid_ref		<= "1110";
		operator		<= "01";
		stall 			<= '1';

		wait until falling_edge(clk);

		assert 	address_offset	= std_logic_vector(to_unsigned(4, AddressWidthData)) and 
				match 			= '0' and
				vr_bin			= std_logic_vector(to_unsigned(1, ClusterWidth)) and
				cluster_match	= "1111" 
			report "Fourth test failed. Wrong result analysis!" severity failure;

		wait until rising_edge(clk);

		-- In this test the third cluster must produce a matching signal
		compare_results <= "0000000100110111";
		valid_ref		<= "1110";
		operator		<= "01";
		stall 			<= '0';

		wait until falling_edge(clk);

		assert 	address_offset	= std_logic_vector(to_unsigned(1, AddressWidthData)) and 
				match 			= '1' and
				vr_bin			= std_logic_vector(to_unsigned(1, ClusterWidth)) and
				cluster_match	= "1101" 
			report "Fifth test failed. Wrong result analysis!" severity failure;

		wait until rising_edge(clk);

		-- also in this case the cluster that matches must be the third one.
		-- address offset must be equal to 3 and and so vr_bin.
		compare_results <= "0011011111101100";
		valid_ref		<= "1110";
		operator		<= "10";
		stall 			<= '0';

		wait until falling_edge(clk);

		assert 	address_offset	= std_logic_vector(to_unsigned(3, AddressWidthData)) and 
				match 			= '1' and
				vr_bin			= std_logic_vector(to_unsigned(3, ClusterWidth)) and
				cluster_match	= "1101" 
			report "Fifth test failed. Wrong result analysis!" severity failure;

		wait until rising_edge(clk);

		-- In this test the first cluste must match and data_offset and vr_bin must be set
		-- to 1
		compare_results <= "0010000000100010";
		valid_ref		<= "1111";
		operator		<= "01";
		stall 			<= '0';

		wait until falling_edge(clk);

		assert 	address_offset	= std_logic_vector(to_unsigned(1, AddressWidthData)) and 
				match 			= '1' and
				vr_bin			= std_logic_vector(to_unsigned(1, ClusterWidth)) and
				cluster_match	= "0111" 
			report "Sixth test failed. Wrong result analysis!" severity failure;

		wait until rising_edge(clk);

		assert false report "Testbench successful... terminating execution" severity failure;
		wait;
	end process;
end behav;
