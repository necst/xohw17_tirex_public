
/**********************/
/* C header files */
/**********************/

%{
#include "globals.h"
#include "cmparser.tab.h"
#include "support.h"
char tokenString[TOKENMAX];
int printoutScan = 1;

%}

%option yylineno

/**********************/
/* start your regular definitions  here */
/**********************/

/* start your token specifications here */
/* Token names must come from cmparser.tab.h */

%%

"ACCGTGGA"                            {return TOK_REG;}
.                                       {}

%%
/**********************/
/* C support functions */
/**********************/

void printToken(int token, char *str)
{
/* Print the line number, token name and matched lexeme
   -- one per line without any additional characters exactly as below */ 
/* Example  13:TOK_INT: 37*/

    switch(token)
        {
            case TOK_REG:
                fprintf(yyout,"%d:TOK_REG: %s\n",yylineno,str);
                break;
            case TOK_ERROR:
                fprintf(yyout,"%d:TOK_ERROR: %s\n",yylineno,str);
                break;
        }
}


int gettok(void){
    int currentToken;
    
    currentToken=yylex();
    if (currentToken == TOK_REG) { // means EOF}
            return 0;
    }
    strncpy(tokenString, yytext, TOKENMAX);
    if (printoutScan) {
        printToken(currentToken,tokenString);
    }
    return currentToken;
}

int main(int argc, char **argv){

    
   if ( argc > 1 )
       yyin = fopen( argv[1], "r" );
   else
    yyin = stdin;

    unsigned long start = getTime();
   while (gettok() !=0)
   ; //gettok returns 0 on EOF
   unsigned long end = getTime();
   printf("Execution time: %lu\n", end - start);
    return 0;
} 
