#ifndef BISON_CMPARSER_TAB_H
# define BISON_CMPARSER_TAB_H

# ifndef YYSTYPE
#  define YYSTYPE int
#  define YYSTYPE_IS_TRIVIAL 1
# endif
# define	TOK_REG	257
# define	TOK_ERROR	284


extern YYSTYPE yylval;

#endif /* not BISON_CMPARSER_TAB_H */
