#! /usr/bin/env python

import getopt, sys, string

def ascii_to_bin(string):
    s = []
    for char in string:
        ascii = ord(char)
        bin = []

        while (ascii > 0):
            if (ascii & 1) == 1:
                bin.append("1")
            else:
                bin.append("0")
            ascii = ascii >> 1

        bin.reverse()
        binary = "".join(bin)
        zerofix = (8 - len(binary)) * '0'
        s += zerofix + binary

    return "".join(s)


class OpCode2Bin:
    def str_call_op(self,s):
        r = []
        if s == "(":
            r = "1"
        else:
            r = "0"
        return r

    def str_int_op(self,s):
        r = []
        if s == "|":
            r = "01"
        elif s == "&":
            r = "10"
        elif s == ".":
            r = "11"
        else:
            r = "00"
        return r

    def str_ext_op(self,s):
        r = []
        if s == ")":
            r = "100"
        elif s == ")*":
            r = "001"
        elif s == ")+":
            r = "010"
        elif s == ")|":
            r = "011"
        elif s == "jmp":
            r = "111"
        else:
            r = "000"
        return r
    
class ReOperation:
    def __init__(self, call_op, int_op, ext_op, reference, cpu_width, asciiconvert, line_width):
        self.reference = reference
        self.int_op = int_op
        self.ext_op = ext_op
        self.call_op = call_op
        self.cpu_width = cpu_width
        self.asciiconvert = asciiconvert
        self.line_width = line_width

    def get_call_op(self):
        return self.call_op

    def get_int_op(self):
        return self.int_op

    def get_ext_op(self):
        return self.ext_op

    def get_reference(self):
        return self.reference

    def format_reference(self):
        if self.asciiconvert:
            if (len(self.reference) < self.cpu_width):
                real_reference = ascii_to_bin(self.reference).ljust(self.line_width,"0")
            else:
                real_reference = ascii_to_bin(self.reference).rjust(self.line_width,"0")
        else:
            real_reference = self.reference.rjust(self.line_width,"0")
        return real_reference

    def binary(self):
        op = OpCode2Bin()

        return self.format_reference() +"\n" + (op.str_call_op(self.call_op) + op.str_int_op(self.int_op) \
            + op.str_ext_op(self.ext_op)).rjust(self.line_width,"0")

    def hexd(self):
        op = OpCode2Bin()

        return hex(int(self.format_reference(), 2)) + ", " + hex(int((op.str_call_op(self.call_op) + op.str_int_op(self.int_op) \
            + op.str_ext_op(self.ext_op)).rjust(self.line_width,"0"),2))

def usage():
    print ("TiReX Compiler ver 2.0")
    print ("by Marco Paolieri, Davide Conficconi\n")
    print ("options:")
    print (" -h|[-m <memory_width> [-c|-d]  -w <regexcpu_width> -l <memory_line_width> --hex <hexadecimal_translation> -i <input_file> -o <output_file>]")
    print ("--help|[--mem_width --compile|--data_mem --width_regexcpu --line_width --hex --input --output]")

def generate_instr_ram_file(IR_list, output_file, address_width, line_width, hexadecimal):
    print ("Writing output_file " + output_file)
    total_address = 2**address_width
    f_out = open(output_file,'w')

    if hexadecimal:
        f_out.write("#ifndef SRC_TIREX_CORE_SDK_H_\n#define SRC_TIREX_CORE_SDK_H_\n\nu32 instr[][2] = {\n")
    current_address = 0
    
    for ir_op in IR_list:
        if not(hexadecimal):
            f_out.write(ir_op.binary()+"\n")
        else:
            f_out.write("{" + ir_op.hexd() +"},\n")
        current_address += 1
        if (current_address > total_address):
            print("Error! we are writing more locations than available!!")
            sys.exit()

    nop = ''.join(["0" for i in range(line_width)])
    starting = ''
    ending = ''
    final = ''
    if not(hexadecimal):
        separator = "\n"
    else:
        nop = hex(int(nop,2))
        separator = ", "
        starting = "{"
        ending = "},"
        final = "}"

    for i in range(current_address, total_address - 1):
        f_out.write (starting + nop + separator)
        f_out.write (nop + ending + "\n")
    f_out.write (starting + nop + separator)
    f_out.write (nop + final)
        
    if hexadecimal:
        f_out.write("};\n\n#endif")
    f_out.close()
        
    print ("Operation Finished")

def generate_data_ram_file(input_file, output_file):
    print ("Reading input_file " + input_file )
    print ("Writing output_file " + output_file)

    f_in = open(input_file,'r')
    f_out = open(output_file, 'w')
    counter = 0

    f_out.write("memory_initialization_radix=16;\nmemory_initialization_vector=\n")
    c = f_in.read(1)
    while (True):
        if not c:
            break
        if(c != "\n"):
            #f_out.write(ascii_to_bin(c))
            f_out.write(c.encode("hex"))
            counter += 1
            c = f_in.read(1)
            if(counter % 4 == 0 and c):
                f_out.write(",\n")
                counter = 0
        else:
            c = f_in.read(1)
    f_out.write(";")    
    f_in.close()
    f_out.close()
    print("Operation Finished")

def isReference(reg_ex,curr_char):
    return ((reg_ex[curr_char] in string.ascii_letters) or (reg_ex[curr_char] in string.digits) \
     or (reg_ex[curr_char]==" ") or (reg_ex[curr_char] in string.punctuation)) and not(reg_ex[curr_char]=="|") \
    and not(reg_ex[curr_char]==")") and not(reg_ex[curr_char]=="(")
    
def build_ir_list(reg_ex, width, line_width):
        ir_operations = []
        par_list = []
        loop_exit = False
        curr_char = 0
        instruction_counter = 0
        jump_flag = False
        int_op_tmp = ""

        while (not(loop_exit)):
            asciiconvert = True
            call_op = ""
            int_op = ""
            ext_op = ""
            reference = "" 
          
            #In case of "\n"
            if reg_ex[curr_char] == "\n":
                loop_nest_exit = True
                break
            #case of open parenthesis
            elif (reg_ex[curr_char] == "("):
                call_op = "("
                curr_char += 1
                asciiconvert = False
                par_list.append(instruction_counter)
#            if (curr_char == len(reg_ex)):
#                loop_exit = True

            else:

#case of inter operand and reference


                loop_nest_exit = False
                while(not(loop_nest_exit)):
                    if(isReference(reg_ex,curr_char)):
                        reference = reference + reg_ex[curr_char]
                        curr_char += 1
                        if(curr_char == len(reg_ex)):
                           loop_nest_exit = True
                        if ( curr_char + 1 < len(reg_ex) and reg_ex[curr_char + 1] == "|" ):
                            loop_nest_exit = True
                        if (int_op == "|" and curr_char + 1 < len(reg_ex) and \
                            (reg_ex,curr_char + 1) and isReference(reg_ex,curr_char)) :
                            loop_nest_exit = True
                        if (not(loop_nest_exit) and reg_ex[curr_char] == "|" ):
                                curr_char += 1
                                int_op = "|"
                                if(curr_char == len(reg_ex)):
                                    loop_nest_exit = True

                        if(len(reference)== width ):
                            loop_nest_exit = True
                    else:
                        loop_nest_exit = True
                if(len(reference) > 0 and int_op==""):
                    int_op = "&"
                
                if (curr_char == len(reg_ex)):
                    loop_exit = True

                else:    #case of external operand

                    if (reg_ex[curr_char:curr_char+2] == ")*"):
                        ext_op = ")*"
                        curr_char += 2
                    elif (reg_ex[curr_char:curr_char+2] == ")+"):
                        ext_op = ")+"
                        curr_char += 2

                    elif (reg_ex[curr_char:curr_char+2] == ")|"):
                        par_list.append(instruction_counter)
                        jump_flag = True
                        ext_op = ")|"
                        curr_char += 2

                    elif (reg_ex[curr_char] == ")"):
                        par_list.append(instruction_counter)
                        ext_op = ")"
                        curr_char += 1

            if (curr_char == len(reg_ex)):
                loop_exit = True
            int_op_tmp = int_op
            print ("Instruction Added: " + call_op + " " + int_op + " " + ext_op + " " + reference) 
            ir_operations.append(ReOperation(call_op,int_op,ext_op, reference,width,asciiconvert, line_width))
            instruction_counter += 1
        if (jump_flag) :
            #print("JUMP AROUND")
            x = 0
            go_on = 0
            while x < len(par_list):
                primary = par_list[x]
                cplx_par_flag = False
                if (ir_operations[primary].call_op == "("):
                    for y in range(x+1,len(par_list)):
                        #print("y = " + str(y) + "\n")
                        index = par_list[y]
                        call = ir_operations[index].call_op
                        element = ir_operations[index].ext_op
                        if (cplx_par_flag and call == "("):
                            ir_operations[index].reference = str('{0:04b}'.format(1))
                        if (not(cplx_par_flag) and (element == ")" or element == ")+" or element == ")*")):
                            break
                        elif(not (cplx_par_flag)):
                            cplx_par_flag = True
                        if (element == ")" and cplx_par_flag):
                            go_on = y
                            ir_operations.insert(primary,ReOperation("","","jmp",str('{0:08b}'.format(index + 2)),width,False, line_width))
                            print ("Instruction Added: " + "jmp" + " offset: " + str(index + 2))
                if (cplx_par_flag and primary + 1 < len(ir_operations)):
                    x = go_on
                    ir_operations[primary + 1].reference = str('{0:04b}'.format(1))
                x += 1
        #for x in xrange(0,len(ir_operations)):
        #    print(ir_operations[x].call_op + ir_operations[x].int_op + ir_operations[x].ext_op + ir_operations[x].reference + "\n")
        return ir_operations

def main():
    try:
        opts, args = getopt.getopt(sys.argv[1:], "hcdm:w:l:hex:i:o:", ["help", "mem_width=", "data_mem", "compile",  \
            "program_mem_width=","width_regexcpu=","line_width=", "hex", "input=","output="])
    except getopt.GetoptError:
        # print help information and exit:
        usage()
        sys.exit(2)

    data_memory_write = False
    mem_width = 0
    input_file = []
    output_file = []
    compile_regex = False
    cpu_width = 0
    line_width = 0
    hexadecimal = False
    
    for o, a in opts:
        if o in ("-h", "--help"):
            usage()
            sys.exit()  
        if o in ("-m", "--mem_width"):
            mem_width = int(a)
            print ("Memory width " + a)
        if o in ("-c", "--compile"):
            compile_regex = True
            print ("Compile ")
        if o in ("-d", "--data_mem"):
            data_memory_write = True
            print ("Data_mem")
        if o in ("-w", "--width_regexcpu"):
            print ("TiReX Cluster Width" + a )
            cpu_width = int(a)
        if o in ("-l", "--line_width"):
            line_width = int(a)
            print("Memory line width " + a)
        if o in ("--hex"):
            hexadecimal = True
            print("Hexadecimal translation\n") 
        if o in ("-i", "--input"):
            print ("Input file:" + a )
            input_file = a
        if o in ("-o", "--output"):
            print ("Output file: " + a )
            output_file = a

    if ( not(input_file) or not(output_file) or (not(compile_regex) and not(data_memory_write)) \
        or (compile_regex and data_memory_write) or (compile_regex and not(mem_width)) or (compile_regex and not(cpu_width)) \
        or (compile_regex and not(line_width))):
        print("ATTENTION: Some required parameters have not been given in input!!")
        usage()
        sys.exit()

    if (data_memory_write == True):
        generate_data_ram_file(input_file,output_file)
    else:
        f_in = open(input_file,'r')
        re = f_in.readline()
        f_in.close()
        ir_operations = build_ir_list(re, cpu_width, line_width)

        generate_instr_ram_file (ir_operations, output_file, mem_width ,line_width, hexadecimal)
        

if __name__=="__main__":
    main()
